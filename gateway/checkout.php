<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is a one-line short description of the file.
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package   enrol_payment
 * @copyright 2020 LMS Doctor <andres.ramos@lmsdoctor.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/enrollib.php');
require_once(__DIR__ . '/../vendor/autoload.php');
require_login();

use enrol_payment\helper;


global $DB, $CFG, $USER;

function moodle_url(string $url, array $params = []) {
    global $CFG;
    return $CFG->wwwroot . "$url?" . http_build_query($params);
}

$stripe = new \Stripe\StripeClient(get_config('enrol_payment', 'stripesecretkey'));
header('Content-Type: application/json');

$inputs = new stdClass;
$inputs->sessionid = required_param('sessionid', PARAM_TEXT);
$inputs->instanceid = required_param('instanceid', PARAM_INT);
$inputs->courseid = required_param('courseid', PARAM_INT);
$inputs->prepaytoken = required_param('prepaytoken', PARAM_TEXT);
$inputs->currency = required_param('currency', PARAM_TEXT);
$inputs->coursename = required_param('coursename', PARAM_TEXT);

$inputs->price = required_param('price', PARAM_FLOAT);
$inputs->subtotal = required_param('subtotal', PARAM_FLOAT);
$inputs->discountamount = required_param('discountamount', PARAM_FLOAT);
$inputs->discountsubtotal = required_param('discountsubtotal', PARAM_FLOAT);
$inputs->discounttotal = required_param('discounttotal', PARAM_FLOAT);
$inputs->taxamount = required_param('taxamount', PARAM_FLOAT);
$inputs->taxsubtotal = required_param('taxsubtotal', PARAM_FLOAT);
$inputs->taxtotal = required_param('taxtotal', PARAM_FLOAT);
$inputs->userids = required_param('userids', PARAM_TEXT);
$inputs->units = required_param('units', PARAM_INT);

$session = $DB->get_record('enrol_payment_session', array('id' => $inputs->sessionid, 'userid' => $USER->id));
if (empty($session)) {
    throw new Exception("Invalid request", 1);
}

// Currency_code = code money
// Units = number of buyers
// Tax = purchase tax
// Price = price with discount or price item
// Pricetotal = prece for units
// Taxtotal = tax for units.

$discount = $inputs->discountamount;
$tax = $inputs->taxamount;
$currency = $inputs->currency;
$quantity = $inputs->units;

// Base price of item
// If-> discounted item price.
$price = $inputs->price;
if ($discount) {
    $price = $inputs->discountsubtotal;
}
if ($tax) {
    $price = $inputs->taxsubtotal;
}

// Price multiplied by units
// If-> discounted price multiplied by units.
$pricetotal = $inputs->subtotal;
if ($discount) {
    $pricetotal = $inputs->discounttotal;
}
// Tax multiplied by units.
$taxtotal = format_float(($tax * $quantity), 2, true);
// Total amount to pay.
$total = $inputs->subtotal;
if ($discount) {
    $total = $inputs->discounttotal;
}
if ($tax) {
    $total = $inputs->taxtotal;
}

$ids = explode(',', $inputs->userids);
if (is_array($ids)) {
    $courses = $DB->get_fieldset_sql("SELECT email from {user} WHERE id IN ($inputs->userids)");
    $usernames = implode(', ', $courses);
} else {
    $usernames = $DB->get_field('user', 'email', array('id' => $inputs->userids));
}

$lineitems = [
    'quantity' => $quantity,
    'price_data' => [
        'currency' => strtolower($currency),
        'unit_amount_decimal' => ($price * 100),
        'product_data' => [
            'name' => $inputs->coursename,
            'description' => "Course purchased by: $usernames"
        ]
    ]
];

$checkout = $stripe->checkout->sessions->create([
    'line_items' => [$lineitems],
    'mode' => 'payment',
    'success_url' => moodle_url('/enrol/payment/gateway/success.php', ['sessionid' => $inputs->sessionid]),
    'cancel_url' => moodle_url('/enrol/index.php', ['id' => $inputs->courseid]),
    'client_reference_id' => $inputs->prepaytoken,
]);

$transaction = new stdClass;
$transaction->instanceid = $inputs->instanceid;
$transaction->sessionid = $inputs->sessionid;
$transaction->courseid = $inputs->courseid;
$transaction->userid = $USER->id;
$transaction->value = $pricetotal;
$transaction->tax = $taxtotal;
$transaction->units = $quantity;
$transaction->userids = $inputs->userids;
$transaction->gatewayid = $checkout->id;
$transaction->gateway = "stripe";
$transaction->status = $checkout->status;
$transaction->type = $checkout->mode;
$transaction->createdat = time();
$transaction->updatedat = time();

// Store the transaction.
$save = $DB->insert_record('enrol_payment_transaction', $transaction);


header("HTTP/1.1 303 See Other");
header("Location: " . $checkout->url);
