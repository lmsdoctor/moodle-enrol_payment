<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is a one-line short description of the file.
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package   enrol_payment
 * @copyright 2020 LMS Doctor <andres.ramos@lmsdoctor.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Disable moodle specific debug messages and any errors in output,
// comment out when debugging or better look into error log!
define('NO_DEBUG_DISPLAY', true);

// @codingStandardsIgnoreLine This script does not require login.
require(__DIR__ . '/../../../config.php');
require_once("lib.php");
require_once($CFG->libdir . '/enrollib.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->dirroot . '/group/lib.php');
require_once($CFG->libdir . '/adminlib.php');

use enrol_payment\helper;
use enrol_payment\paypalipn;

$ipn = new paypalipn();

// Set this to true to use the sandbox endpoint during testing.
$enablesandbox = get_config('enrol_payment', 'enablesandbox');

// Use the sandbox endpoint during testing.
if ($enablesandbox) {
    $ipn->usesandbox();
}

$verified = $ipn->verifyipn();

// PayPal does not like when we return error messages here,
// the custom handler just logs exceptions and stops.

// Make sure we are enabled in the first place.
if (!enrol_is_enabled('payment')) {
    http_response_code(503);
    throw new moodle_exception('errdisabled', 'enrol_payment');
}

$txn = new stdClass;
$txn->business = optional_param('business', '', PARAM_TEXT);
// This is the prepaytoken coming from paypal in the custom field.
$txn->token = optional_param('custom', '', PARAM_TEXT);
$txn->optionname1 = optional_param('option_name1', '',  PARAM_TEXT);
$txn->optionselection1x = optional_param('option_selection1', '',  PARAM_TEXT);
$txn->paymentstatus = required_param('payment_status', PARAM_TEXT);
$txn->paymenttype = required_param('payment_type', PARAM_TEXT);
$txn->receiveremail = required_param('receiver_email', PARAM_TEXT);
$txn->receiverid = required_param('receiver_id', PARAM_TEXT);
$txn->tax = optional_param('tax', '', PARAM_TEXT);
$txn->txnid = required_param('txn_id', PARAM_TEXT);
$txn->payment_gross = optional_param('mc_gross', '', PARAM_TEXT);
$txn->mc_currency = optional_param('mc_currency', '', PARAM_TEXT);

$filehandle = fopen("$CFG->wwwroot/enrol/payment/ipn.text", 'a+');
fwrite($filehandle, json_encode((array)$_POST));
fwrite($filehandle, "\n");
fwrite($filehandle, '___');
fclose($filehandle);
// Stop if if the custom (prepaytoken) is not present, as it is understood
// the product purchased was not a moodle course.
if (empty($txn->token)) {
    header('HTTP/1.1 200 OK');
    die();
}

$coupon = new stdClass;
$coupon->amount = 0;
$coupon->type = 0;
$coupon->threshold = 0;

$payment = helper::get_payment_from_token($txn->token);

if (empty($payment)) {
    throw new moodle_exception('invalidrequest', 'core_error', '', null, "Invalid value of prepay token: $txn->token");
}

// Discount coupon validation.
if ($payment->code) {

    // Get coupon config.
    $select = sprintf("%s = :code", $DB->sql_compare_text('code'));
    $coupon = $DB->get_record_select('enrol_payment_discountcode', $select, array('code' => trim($payment->code)));
    $iscoupon = empty($coupon) || empty($coupon->type);
    $istype = !$iscoupon && intval($coupon->type <= 0);
    $errormsg = get_string('incorrectdiscountcode_desc', 'enrol_payment');
    // If the coupon is invalid.
    if ($iscoupon || $istype) {
        throw new moodle_exception('incorrectdiscountcode', 'core_error', '', null, $errormsg);
    }
    // If the coupon exceeded the limit of use.
    $isusagelimit = !empty($coupon->usagelimit) && intval($coupon->usagelimit) <= 0;
    if ($isusagelimit) {
        throw new moodle_exception('codeusagelimit', 'core_error', '', null, $errormsg);
    }
    $payment->codegiven = true;
}

unset($txn->token);

$txn->userid = (int)$payment->userid;
$txn->courseid = (int)$payment->courseid;
$txn->instanceid = (int)$payment->instanceid;
$txn->timeupdated = time();

$multiple = (bool)$payment->multiple;
if ($multiple) {
    $multipleuserids = explode(',', $payment->multipleuserids);
    if (empty($multipleuserids)) {
        throw new moodle_exception(
            'invalidrequest',
            'core_error',
            '',
            null,
            "Multiple purchase specified, but no userids found."
        );
    }
}

$user = $DB->get_record("user", array("id" => $txn->userid), "*", MUST_EXIST);
$course = $DB->get_record("course", array("id" => $txn->courseid), "*", MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);
$txn->itemname = $course->fullname;

$PAGE->set_context($context);

$plugininstance = $DB->get_record("enrol", array("id" => $txn->instanceid, "enrol" => "payment", "status" => 0));
$plugin = enrol_get_plugin('payment');

// If the connection is not OK, save the transaction and throw a moodle exception.
if (!$verified) {
    $DB->insert_record("enrol_payment_transaction", $txn, false);
    throw new moodle_exception('erripninvalid', 'enrol_payment', '', null, json_encode($txn));
}

// Check the payment_status and payment_reason
// If status is not completed or pending then unenrol the student if already enrolled and notify admin.
if ($txn->paymentstatus != "Completed"  && $txn->paymentstatus != "Pending") {
    if (!empty($multiple)) {
        foreach ($multipleuserids as $muid) {
            $plugin->unenrol_user($plugininstance, $muid);
        }
    } else {
        $plugin->unenrol_user($plugininstance, $txn->userid);
    }
    helper::message_paypal_error_to_admin(get_string('notcompleted', 'enrol_payment'));
    die();
}

// If status is pending and reason is other than echeck then we are on hold until further notice
// Email user to let them know. Email admin.
if ($txn->paymentstatus == "Pending" && $txn->pendingreason != "echeck") {

    // Notify the user.
    $messagehtml = '<p>' . get_string('paymentpending', 'enrol_payment') . '</p>';
    $messagetext = html_to_text($messagehtml);
    $fromuser = self::get_fromuser_object();
    email_to_user($user, $fromuser, $subject, $messagetext, $messagehtml, ', ', false);

    // Notify the admin.
    helper::message_paypal_error_to_admin(get_string('paymentpending', 'enrol_payment'));

    $DB->insert_record('enrol_payment_transaction', $txn);
    $DB->update_record('enrol_payment_session', array('id' => $payment->id, 'paypaltxnid' => $txn->txnid));
    die();
}

// At this point we only proceed with a status of completed or pending with a reason of echeck.
// Make sure this transaction doesn't exist already.
if ($existing = $DB->get_record('enrol_payment_transaction', array('txnid' => $txn->txnid))) {
    helper::message_paypal_error_to_admin(get_string('txnrepeated', 'enrol_payment', $txn->txnid));
    die();
}

// Check that the receiver email is the one we want it to be.
if (isset($txn->business)) {
    $recipient = $txn->business;
} else if (isset($txn->receiveremail)) {
    $recipient = $txn->receiveremail;
} else {
    $recipient = 'empty';
}

if (strtolower($recipient) !== strtolower($plugin->get_config('paypalbusiness'))) {
    helper::message_paypal_error_to_admin(
        get_string('recipientnotmatch', 'enrol_payment', $recipient)
    );
    die();
}

$coursecontext = context_course::instance($course->id, IGNORE_MISSING);

// Check that amount paid is the correct amount.
if ((float) $plugininstance->cost <= 0) {
    $originalcost = (float) $plugin->get_config('cost');
} else {
    $originalcost = (float) $plugininstance->cost;
}

// Use the same rounding of floats as on the enrol form.
$originalcost = format_float($originalcost, 2, false);

// What should the user have paid? Verify using info stored in the database.
$costs = helper::calculate_cost($coupon, $payment);
$cost = $costs->subtotal ?? $costs->discounttotal ?? $costs->taxtotal;

if ($txn->payment_gross + 0.01 < $cost) {
    // This shouldn't happen unless the user spoofs their requests, but
    // if it does, the discount is just invalid.
    $str = new stdClass;
    $str->gross = $txn->payment_gross;
    $str->cost = $cost;
    helper::message_paypal_error_to_admin(get_string('amountisnotenough', 'enrol_payment', $str));
    die;
}

// All clear.
$DB->insert_record("enrol_payment_transaction", $txn);
$DB->update_record("enrol_payment_session", array("id" => $payment->id, "paypaltxnid" => $txn->txnid));

if ($plugininstance->enrolperiod) {
    $timestart = time();
    $timeend = $timestart + $plugininstance->enrolperiod;
} else {
    $timestart = 0;
    $timeend = 0;
}

if (empty($multiple)) {
    // Make a singleton array so that we can do this whole thing in a foreach loop.
    $multipleuserids = [$user->id];
}

// Pass $view = true to filter hidden caps if the user cannot see them.
if ($users = get_users_by_capability(
    $context,
    'moodle/course:update',
    'u.*',
    'u.id ASC',
    '',
    '',
    '',
    '',
    false,
    true
)) {
    $users = sort_by_roleassignment_authority($users, $context);
    $teacher = array_shift($users);
} else {
    $teacher = false;
}

foreach ($multipleuserids as $uid) {

    // Check that user exists.
    if (!$user = $DB->get_record('user', array('id' => $uid))) {
        helper::message_paypal_error_to_admin(get_string('usernotexist', 'enrol_payment', $uid));
        die();
    }

    if (!empty($plugininstance->customint1)) {
        // Include the welcome message from the plugin instance.
        $plugin->welcomemessage = $plugininstance->customtext1;
        helper::notification_messages($plugin, $course, $user);
    }

    // If group selection is not null.
    if ($plugininstance->customint2) {
        groups_add_member($plugininstance->customint2, $user);
    }

}

header('HTTP/1.1 200 OK');
