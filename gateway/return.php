<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is a one-line short description of the file.
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package   enrol_payment
 * @copyright 2020 LMS Doctor <andres.ramos@lmsdoctor.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__ . '/../../../config.php');
require_login();

use enrol_payment\helper;
use enrol_payment\paypalipn;

$ipn = new paypalipn();

$id = required_param('id', PARAM_INT);
$token = required_param('custom', PARAM_TEXT);
$transid1 = optional_param('txn_id', "", PARAM_TEXT);
$transid2 = optional_param('PayerID', "", PARAM_TEXT);
$txnid = $transid1 ? $transid1 : $transid2;

$payment = helper::get_payment_from_token($token);
$payment->paypaltxnid = $txnid;
$DB->update_record('enrol_payment_session', $payment);
$purchasingforself = true;

if (!$course = $DB->get_record("course", array("id" => $id))) {
    redirect($CFG->wwwroot);
}

$context = context_course::instance($course->id, MUST_EXIST);
$PAGE->set_context($context);

if (!empty($SESSION->wantsurl)) {
    $destination = $SESSION->wantsurl;
    unset($SESSION->wantsurl);
} else {
    $destination = "$CFG->wwwroot/course/view.php?id=$course->id";
}


if ($payment->multiple) {
    $userids = explode(',', $payment->multipleuserids);
    if (!in_array($USER->id, $userids, true)) {
        $purchasingforself = false;
    }
}

$fullname = format_string($course->fullname, true, array('context' => $context));

if ($purchasingforself) {
    if (is_enrolled($context, null, '', true)) {
        // TODO: use real paypal check.
        redirect($destination, get_string('paymentthanks', '', $fullname));
    } else {
        // IPN is slow, and doesn't always complete immediately...
        $PAGE->requires->css('/enrol/payment/less/styles.css');
        $PAGE->requires->js_call_amd('enrol_payment/return', 'init', array($destination, $course->id, $payment->id));
        $PAGE->set_url($destination);

        echo $OUTPUT->header();
        $a = new stdClass();
        $a->teacher = get_string('defaultcourseteacher');
        $a->fullname = $fullname;
        echo '<div style="text-align: center;" class="paypal-wait">';
        echo $OUTPUT->box(get_string('paypalwait', 'enrol_payment', $course->fullname), 'generalbox', 'notice');
        echo '</div>';
        echo '<div id="spin-container" ></div>';
        echo $OUTPUT->footer();
    }
} else {
    $PAGE->set_url($destination);
    echo $OUTPUT->header();
    echo '<div style="text-align: center;">';
    echo $OUTPUT->box(get_string('thanksforpaypal', 'enrol_payment', $course->fullname), 'generalbox', 'notice');
    echo '</div>';
    echo $OUTPUT->footer();
}
