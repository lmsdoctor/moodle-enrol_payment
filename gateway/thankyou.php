<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is a one-line short description of the file.
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package   enrol_payment
 * @copyright 2020 LMS Doctor <andres.ramos@lmsdoctor.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once(dirname(__FILE__, 4) . '/config.php');
require_once(dirname(__FILE__, 2) . '/global.php');
require_once($CFG->libdir . '/enrollib.php');
require_once($CFG->dirroot . '/group/lib.php');

require_login();

use \core\output\notification;
use \enrol_payment\helper;

global $DB, $USER;

$sessionid = optional_param('sessionid', 0, PARAM_INT);

$tablesession = PAYMENT_DISCOUNT . '_session';
$tabletransaction = PAYMENT_DISCOUNT . '_transaction';

if (empty($sessionid) || $sessionid == 0) {
    throw new Exception('Invalid request', 1);
}

// Get the transaction.
$sql = "SELECT *
        FROM {$tabletransaction}
        WHERE sessionid = :sessionid
                AND userid = :userid
                AND id = (SELECT MAX(id) FROM {$tabletransaction})";
$params = array('sessionid' => $sessionid, 'userid' => $USER->id);
$transaction = $DB->get_record_sql($sql, $params, IGNORE_MISSING);
if (empty($transaction)) {
    throw new Exception('Invalid request', 1);
}


// Transaction record to update the transaction records.
$transaction->status = 'complete';
$transaction->updatedat = time();
$DB->update_record($tabletransaction, $transaction);

// Update the session status.
$params = array('id' => $sessionid, 'userid' => $USER->id, 'status' => 0);
$paymentsession = $DB->get_record($tablesession, $params);
if (empty($paymentsession)) {
    throw new Exception('Invalid payment session', 1);
}

$paymentsession->status = 1;
$DB->update_record($tablesession, $paymentsession);
$userids = explode(',', $transaction->userids);

$course = $DB->get_record("course", array("id" => $transaction->courseid), "*", MUST_EXIST);
$context = context_course::instance($transaction->courseid, IGNORE_MISSING);
$PAGE->set_context($context);

// enrollment plugin instance.
$plugininstance = $DB->get_record('enrol', array('id' => $transaction->instanceid, 'enrol' => 'payment', 'status' => 0));

// Get the enrollment plugin.
$plugin = enrol_get_plugin('payment');

// Loop through each course and enroll the student.
foreach ($userids as $index => $userid) {

    // Set the enrollment period.
    $timestart = 0;
    $timeend = 0;
    if ($plugininstance->enrolperiod) {
        $timestart = time();
        $timeend = $timestart + $plugininstance->enrolperiod;
    }

    if (!$user = $DB->get_record('user', array('id' => $userid))) {   // Check that user exists.
        helper::notification_messages_error_to_admin("User $userid doesn't exist", $transaction);
        die;
    }

    $plugin->enrol_user($plugininstance, $userid, $plugininstance->roleid, $timestart, $timeend, ENROL_USER_ACTIVE);
    // If group selection is not null.
    if ($plugininstance->customint2) {
        groups_add_member($plugininstance->customint2, $user);
    }

    $plugin->welcomemessage = $plugininstance->customtext1;
    helper::notification_messages($plugin, $course, $user);
}

// Store the transaction.
$code = $paymentsession->coupon;
if (!empty($code) && $code) {
    // Get coupon.
    $select = sprintf("%s = :code", $DB->sql_compare_text('code'));
    $coupon = $DB->get_record_select('enrol_payment_discountcode', $select, array('code' => trim($code)));

    if (!empty($coupon) && $coupon) {
        $usagelimit = (int)$coupon->usagelimit;
        $uses = (int)$coupon->uses;
        // Change usage limit.
        if ($usagelimit > 0 && $uses <= $usagelimit) {
            $coupon->uses = $uses + 1;
            $DB->update_record('enrol_payment_discountcode', $coupon);
        }
    }

}

// Redirect back to the home page.
redirect(
    new moodle_url("/my"),
    get_string('thankorder', PAYMENT_DISCOUNT) . $course->fullname,
    0,
    notification::NOTIFY_SUCCESS
);
