<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is a one-line short description of the file.
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package   enrol_payment
 * @copyright 2020 LMS Doctor <andres.ramos@lmsdoctor.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__, 4) . '/config.php');
require_once(dirname(__FILE__, 2) . '/global.php');
require_once(dirname(__FILE__, 2) . '/vendor/autoload.php');
require_once($CFG->libdir . '/enrollib.php');


use \core\output\notification;

require_login();

$type = required_param('type', PARAM_TEXT);
$notification = null;
$message = null;

if ($type == "error") {
    $message = get_string('errororder', PAYMENT_DISCOUNT);
    $notification = notification::NOTIFY_ERROR;
} else if ($type == "success") {
    $course = $DB->get_record('course', array("id" => required_param('id', PARAM_TEXT)));
    $message = get_string('thankorder', PAYMENT_DISCOUNT) . $course->fullname;
    $notification = notification::NOTIFY_SUCCESS;
}

// Redirect back to the home page.
redirect(new moodle_url("/my"), $message, 0, $notification);
