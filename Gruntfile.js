"use strict";

module.exports = function (grunt) {
  // We need to include the core Moodle grunt file too, otherwise we can't run tasks like "amd".
  require("grunt-load-gruntfile")(grunt);
  grunt.loadGruntfile("../../Gruntfile.js");

  // Load all grunt tasks.
  grunt.loadNpmTasks("grunt-contrib-less");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-contrib-uglify");

  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    watch: {
      files: "less/*.less",
      tasks: ["less"],
    },
    less: {
      development: {
        options: {
          paths: ["less/"],
          compress: true,
        },
        files: {
          "styles/styles.css": "less/styles.less",
        },
      },
    },
    // uglify: {
    //   options: {
    //     banner:
    //       '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
    //   },
    //   build: {
    //     src: "amd/src/*.js",
    //     dest: "amd/build/min.js",
    //   },
    // },
  });
  // The default task (running "grunt" in console).
  grunt.registerTask("default", ["less"]);
};
