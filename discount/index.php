<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is a one-line short description of the file.
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package   enrol_payment
 * @copyright 2020 LMS Doctor <andres.ramos@lmsdoctor.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__, 4) . '/config.php');
require_once(dirname(__FILE__, 2) . '/global.php');
require_once(dirname(__FILE__, 2) . '/lib.php');
require_once(dirname(__FILE__) . '/discountcode_form.php');
require("$CFG->libdir/tablelib.php");

require_login();
if (isguestuser()) {
    print_error('guestsarenotallowed');
}

use \core\output\notification;
use enrol_payment\discountcode_table;

global $SESSION, $USER, $DB;

$context = context_system::instance();
$download = optional_param('download', '', PARAM_ALPHA);

if (!has_capability('enrol/payment:manage', $context)) {
    redirect(new moodle_url('/my'), get_string('requiredpermissions', PAYMENT_DISCOUNT), 0, notification::NOTIFY_WARNING);
}

$url = "$CFG->wwwroot/enrol/payment/discount";
$PAGE->set_context($context);
$PAGE->set_url($url);

$table = new discountcode_table('uniqueid');
$table->is_downloading($download, 'DiscountCodes_' . time(), 'discountcodes');

// If the table is not downloading.
if (!$table->is_downloading()) {

    // Define heading and title.
    $PAGE->set_heading(get_string('discountcodes', PAYMENT_DISCOUNT));
    $PAGE->set_title(get_string('discountcodes', PAYMENT_DISCOUNT));

    // Display button.
    echo $OUTPUT->header();
    $button = new moodle_url('action.php', array('id' => '-1'));
    echo $OUTPUT->single_button($button, get_string('newdiscountcode', PAYMENT_DISCOUNT), 'GET', array('class' => 'my-5'));
}

// Work out the sql for the table.
$table->set_sql('*', '{enrol_payment_discountcode}', '1');
$table->define_baseurl($url);
$table->out(40, true);

if (!$table->is_downloading()) {
    echo $OUTPUT->single_button($button, get_string('newdiscountcode', PAYMENT_DISCOUNT), 'GET', array('class' => 'my-5'));
    echo $OUTPUT->footer();
}
