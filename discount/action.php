<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file handle the actions for the discount code form.
 *
 * @package    enrol_payment
 * @copyright  2023 Andres Ramos, Elkin Ch. LMS Doctor
 * @author     LMS Doctor <support@lmsdoctor.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__, 4) . '/config.php');
require_once(dirname(__FILE__, 2) . '/global.php');
require_once(dirname(__FILE__, 2) . '/lib.php');
require_once(dirname(__FILE__) . '/discountcode_form.php');

// Make sure guests can't access to this page.
require_login();
if (isguestuser()) {
    print_error('guestsarenotallowed');
}

use enrol_payment\helper;
use \core\output\notification;

global $DB;

// Read parameters.
$id = required_param('id', PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);

// Set default value.
$context = context_system::instance();

if (!has_capability('enrol/payment:manage', $context)) {
    $msgtext = get_string('requiredpermissions', PAYMENT_DISCOUNT);
    redirect(new moodle_url("/my"), $msgtext, 0, notification::NOTIFY_WARNING);
}

$url = new moodle_url("/enrol/payment/discount");

// Titles and headings.
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($url . '/action.php');
$PAGE->set_heading(get_string('pluginname', PAYMENT_DISCOUNT) . ' / ' . get_string('discountcode', PAYMENT_DISCOUNT));
$PAGE->set_title(get_string('discountcode', PAYMENT_DISCOUNT));

// Disable discount code.
if ($action == 'disable') {
    $DB->update_record('enrol_payment_discountcode', array('id' => $id, 'disabled' => 1));
    redirect($url, get_string('changessaved'), 0, \core\output\notification::NOTIFY_SUCCESS);
}

// Enable discount code.
if ($action == 'enable') {
    $DB->update_record('enrol_payment_discountcode', array('id' => $id, 'disabled' => 0));
    redirect($url, get_string('changessaved'), 0, \core\output\notification::NOTIFY_SUCCESS);
}

// Form pre setup.
$preformdata = array('id' => $id, 'action' => $action);
// If editing.
if ($action == 'edit' && $preformdata = $DB->get_record('enrol_payment_discountcode', array('id' => $id))) {
    $preformdata->action = 'edit';
}

$mform = new discountcode_form(null, (array)$preformdata);

// Form processing and displaying is done here.
if ($mform->is_cancelled()) {
    // Handle form cancel operation, if cancel button is present on form.
    redirect($url);
} else if ($formdata = $mform->get_data()) {

    // Remove additional spaces.
    $formdata->code = trim($formdata->code);

    // When updating.
    if ($formdata->action == 'edit') {

        $data = helper::convert_to_courses($formdata);
        $DB->update_record('enrol_payment_discountcode', $data);
        $status = get_string('changessaved');
    } else {

        // Check if the discount code is duplicated.
        $select = sprintf("%s = :code", $DB->sql_compare_text('code'));
        if ($DB->record_exists_select('enrol_payment_discountcode', $select, array('code' => $formdata->code))) {
            // Redirect back to the coupons page.
            redirect($url, get_string('duplicatecoupon', PAYMENT_DISCOUNT), 0, notification::NOTIFY_ERROR);
        }
        $data = helper::convert_to_courses($formdata);
        $DB->insert_record('enrol_payment_discountcode', $data);
        $status = get_string('changessaved');
    }

    redirect($url, $status, 0, \core\output\notification::NOTIFY_SUCCESS);
} else {

    echo $OUTPUT->header();
    echo heading_action(get_string('discountcode', PAYMENT_DISCOUNT), get_string('goback', PAYMENT_DISCOUNT));

    // Set the form data.
    $mform->set_data($preformdata);
    $mform->display();
    echo $OUTPUT->footer();
}
