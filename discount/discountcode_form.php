<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Discount codes form.
 *
 * @package   enrol_payment
 * @copyright 2020 LMS Doctor <andres.ramos@lmsdoctor.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// Moodleform is defined in formslib.php.
require_once("$CFG->libdir/formslib.php");
require_once($CFG->dirroot . '/lib/enrollib.php');

class discountcode_form extends moodleform {

    // Add elements to form.
    public function definition() {
        global $CFG, $DB;

        $courses = $DB->get_records('course', array('visible' => true), 'shortname ASC');

        // Add "float2" element for float formatting.
        require_once('HTML/QuickForm.php');
        MoodleQuickForm::registerElementType(
            'float2',
            dirname(__FILE__) . '/../classes/float2.php',
            "MoodleQuickForm_float2"
        );
        // Don't forget the underscore!.
        $mform = $this->_form;

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_TEXT);

        // Discount type.
        $options = array(
            1 => get_string('percentdiscount', 'enrol_payment'),
            2 => get_string('valuediscount', 'enrol_payment'),
        );

        $mform->addElement('select', 'type', get_string('coupontype', 'enrol_payment'), $options);

        // Discount amount.
        $mform->addElement('float2', 'amount', get_string('couponamount', 'enrol_payment'));
        $mform->setType('amount', PARAM_RAW);
        $mform->disabledIf('amount', 'type', 'eq', 0);

        // Discount code - text.
        $mform->addElement('text', 'code', get_string('couponcode', 'enrol_payment'));
        $mform->setType('code', PARAM_TEXT);
        $mform->setDefault('code', '');
        $mform->disabledIf('code', 'type', 'eq', 0);

        // Courses.
        $options = array(
            0 => get_string('allcourses', 'enrol_payment'),
        );

        foreach ($courses as &$value) {
            $options[$value->id] = $value->shortname;
        }
        $attributes = array('size' => 12);
        $mform->addElement('select', 'course', get_string('couponcourse', 'enrol_payment'), $options, $attributes);
        $mform->getElement('course')->setMultiple(true);
        $mform->addHelpButton('course', 'courses', 'enrol_payment');

        $mform->addElement('advcheckbox', 'isthreshold', get_string('requirethreshold', 'enrol_payment'));
        $mform->addHelpButton('isthreshold', 'requirethreshold', 'enrol_payment');
        $mform->setType('isthreshold', PARAM_INT);
        $mform->setDefault('isthreshold', 0);

        // Discount threshold.
        $mform->addElement('text', 'threshold', get_string('couponthreshold', 'enrol_payment'));
        $mform->setType('threshold', PARAM_INT);
        $mform->setDefault('threshold', 1);

        // $mform->disabledIf('threshold', 'type', 'eq', 0);
        $mform->disabledIf('threshold', 'isthreshold', 'eq', 0);
        $mform->addHelpButton('threshold', 'couponthreshold', 'enrol_payment');

        // Discount usage limit.
        $mform->addElement('text', 'usagelimit', get_string('couponusagelimit', 'enrol_payment'));
        $mform->setType('usagelimit', PARAM_INT);
        $mform->setDefault('usagelimit', "");
        $mform->disabledIf('usagelimit', 'type', 'eq', 0);
        $mform->addHelpButton('usagelimit', 'couponusagelimit', 'enrol_payment');

        $mform->disabledIf('submitbutton', 'amount', 'eq', 0);
        $mform->disabledIf('submitbutton', 'code', 'eq', '');

        if (!empty($this->_customdata['id'] && $this->_customdata['id'] != '-1')) {
            $this->add_action_buttons(true, get_string('update'));
        } else {
            $this->add_action_buttons(true, get_string('save'));
        }
    }

    // Custom validation should be added here.
    public function validation($data, $files) {

        $errors = array();
        $type = $this->validation_input('type', $data);
        $code = $this->validation_input('code', $data);
        $amount = $this->validation_input('amount', $data);
        $requiredthreshold = $this->validation_input('isthreshold', $data);
        $threshold = $data['threshold'];
        $usagelimit = $this->validation_input('usagelimit', $data);

        if (!$type) {
            $errors['type'] = get_string('discounttypeerror', 'enrol_payment');
        }

        if (!$amount) {
            $errors['amount'] = get_string('needcouponamount', 'enrol_payment');
        }

        // If discount code is filled, discount amount must be filled.
        if (!$code) {
            $errors['code'] = get_string('needdiscountcode', 'enrol_payment');
        }

        // Ensure Discount Threshold is greater than 1.
        if (!empty($requiredthreshold) && $threshold <= 1) {
            $errors['threshold'] = get_string('couponthresholdtoolow', 'enrol_payment');
        }

        if ($threshold) {
            $val = $this->validation_number('threshold', $data);
            if (!$val->isnumeric) {
                $errors['threshold'] = 'The value is not a number';
            }
            if ($data['threshold'] <= 0) {
                $errors['threshold'] = get_string('negativethresholdterror', 'enrol_payment');
            }
        }

        if ($usagelimit) {
            $val = $this->validation_number('usagelimit', $data);
            if (!$val->isnumeric) {
                $errors['usagelimit'] = get_string('discountamounterror', 'enrol_payment');
            }
            if ($data['usagelimit'] <= 0) {
                $errors['usagelimit'] = get_string('negativeusagelimitterror', 'enrol_payment');
            }
        }

        if ($amount) {
            $val = $this->validation_number('amount', $data);
            if (!$val->isnumeric) {
                $errors['amount'] = get_string('discountamounterror', 'enrol_payment');
            }

            // Display error if the value is negative.
            if ($data['amount'] <= 0) {
                $errors['amount'] = get_string('negativediscounterror', 'enrol_payment');
            }

            $totaldigits = strlen(str_replace('.', '', $val->value));
            $digitsafterdecimal = strlen(substr(strrchr($val->value, "."), 1));
            if ($totaldigits > 12 || $digitsafterdecimal > 7) {
                $errors['amount'] = get_string('discountdigitserror', 'enrol_payment');
            }
        }

        // Validate if the percentage value is greater than 100.
        if ($data['type'] == 1 && $data['amount'] > 100) {
            $errors['amount'] = get_string('percentageexceedsmaximumerror', 'enrol_payment');
        }


        return $errors;
    }

    // Custom validation should be added here.
    public function validation_input(string $input, array $data) {
        return array_key_exists($input, $data) && !empty($data[$input]);
    }

    // Is numeri.
    public function validation_number(string $input, array $data) {
        $value = str_replace(get_string('decsep', 'langconfig'), '.', $data[$input]);
        $vl = new stdClass();
        $vl->value = $value;
        $vl->isnumeric = is_numeric($value);
        return $vl;
    }
}
