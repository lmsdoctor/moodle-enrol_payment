/**
 * Enrol page JS for Payment plugin
 *
 * @package
 * @copyright 2020 LMS Doctor <andres.ramos@lmsdoctor.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define([
  "jquery",
  "core/modal_factory",
  "core/modal_events",
  "core/str",
  "core/config",
  "enrol_payment/spin",
  "core/ajax",
  "core/notification",
], function (
  $,
  ModalFactory,
  ModalEvents,
  MoodleStrings,
  MoodleCfg,
  Spinner,
  Ajax,
  Notification
) {
  /**
   * JavaScript functionality for the enrol_payment enrol.html page
   */
  var EnrolPage = {
    /**
     * Query select UI
     */
    dateRef: $("div.data-ref"),
    trDiscountError: $(".tr-discount-error"),
    trEnrollmentError: $(".tr-enrollment-error"),

    trDiscount: $(".tr-discount"),
    spanDiscount: $(".span-discount"),
    spanDiscountAlert: $(".span-discount-alert"),
    smallDiscount: $(".small-discount"),
    boldDiscount: $(".bold-discount"),
    spanDiscountamount: $(".span-discountamount"),
    spanDiscountsubtotal: $(".span-discountsubtotal"),

    thDiscountValue: $(".th-discount-value"),
    thDiscountPorcentage: $(".th-discount-porcentage"),

    trThreshold: $(".tr-threshold"),
    spanThreshold: $(".span-threshold"),
    spanWillApply: $(".span-will-apply"),
    spanDiscountApply: $(".span-discount-apply"),
    trTotalamount: $(".tr-totalamount"),
    tdTotalamount: $(".td-totalamount"),
    spanTaxTotal: $(".span-taxtotal"),
    spanTaxSubtotal: $(".span-taxsubtotal"),
    spanTaxAmount: $(".span-taxamount"),
    bankTransferCost: $("#banktransfer-cost"),
    spanUnits: $(".span-units"),
    directenrollment: false,
    alertSuccess: undefined,
    alertError: undefined,
    discountcode: undefined,
    discountContainer: undefined,
    unitDiscount: undefined,

    trBuyNowAction: $("tr.tr-buynow-action"),
    trGatewayAction: $("tr.tr-gateway-action"),

    formBuynow: $("form#buynow-form"),
    formStripe: $("form#stripe-form"),
    formPaypal: $("form#paypal-form"),
    formDiscountCode: $("form#discountcode-from"),

    formEnrollment: $("form#enrollment-from"),
    tbodyEnrollment: $(".tbody-enrollment"),
    trEnrollmentUser: $(".tr-enrollment-user"),
    trEnrollmentForm: $(".tr-enrollment-form"),
    tbodyEnrollmentList: $(".tbody-enrollment-list"),

    spanBankTransferInfo: $(".banktransferinfo"),
    pPaypalInfo: $(".paypalinfo"),
    /**
     * Set at init time. Moodle strings
     */
    mdlstr: undefined,
    stringkeys: [],
    /**
     * The payment gateway that will be used. Either "paypal" or "stripe"
     */
    gateway: null,

    /**
     * Set at init time.
     */
    price: undefined,

    /**
     * Subtotal used for purchase.
     */
    subtotal: 0,

    /**
     * Tax as a percentage of the purchase subtotal
     */
    tax: undefined,

    /**
     * Dollar tax amount
     */
    taxamount: undefined,

    /**
     * ID of this enrollment instance. Set at init time.
     */
    instanceid: undefined,

    /**
     * Unique ID for this page visit
     */
    prepaytoken: undefined,

    /**
     * Billing address required
     */
    billingaddressrequired: undefined,

    /**
     * Should stripe validate zip code
     */
    validatezipcode: undefined,

    /**
     * Require user to enter shipping address?
     */
    shippingaddressrequired: undefined,

    /**
     * User's email address
     */
    useremail: undefined,

    /**
     * Instance currency string
     */
    currency: undefined,

    /**
     * Currency symbol (currently only $ supported)
     */
    symbol: undefined,

    /**
     * Does the user need a code in order to get a discount?
     */
    discountcoderequired: undefined,

    /**
     *
     */
    stripepublishablekey: undefined,

    codegiven: false,
    coderequired: false,
    discount: 0,
    discountamount: 0,
    discountisvalue: 0,
    discountsubtotal: 0,
    discounttotal: 0,
    discounttype: 0,
    discounttype1: 0,
    discounttype2: 0,
    hastax: false,
    taxsubtotal: 0,
    taxtotal: 0,

    userids: "",

    threshold: undefined,

    units: undefined,

    ServiceAjaxRequest: function ({
      method = "",
      body = {},
      then = () => {},
      error = () => {},
    }) {
      Ajax.call([
        {
          methodname: `enrol_payment_${method}`,
          args: body,
          done: function (response) {
            var json = JSON.parse(response);
            if (json?.error) {
              error(json.errormsg);
            }
            if (json?.error) {
              return;
            }
            then(json);
          }.bind(this),
          fail: Notification.exception,
        },
      ]);
    },

    checkDiscountCode: function (args) {
      var self = this;
      // Always clean this div first.
      self.trThreshold.addClass("d-none");
      self.trDiscountError.addClass("d-none");

      self.ServiceAjaxRequest({
        method: "check_discount",
        body: {
          ...args,
          enrolid: self.instanceid,
          prepaytoken: self.prepaytoken,
        },
        error: function (msg) {
          self.errorFrom(msg, "trDiscountError");
        },
        then: function (res) {
          self.setValueOfObject(res);
          self.statusForm("formDiscountCode", true);
          self.trThreshold.removeClass("d-none");
          // Hide the discount-container.
          self.updateCostView();
        },
      });
    },

    checkEnrollmentEmail: function (email, action = 1) {
      var self = this;
      email = decodeURIComponent(email);
      email = email.replace(/\s/g, "");
      // Check if the email address contains "mailto:"
      if (email && email.startsWith("mailto:")) {
        // Remove the "mailto:" prefix
        email = email.substring(7);
      }

      self.ServiceAjaxRequest({
        method: "check_enrollment_email",
        body: {
          email,
          enrolid: self.instanceid,
          prepaytoken: self.prepaytoken,
          action,
        },
        error: function (msg) {
          self.errorFrom(msg, "trEnrollmentError");
        },
        then: function (res) {
          self.trEnrollmentError.addClass("d-none");
          self.setValueOfObject(res);
          if (res?.user) {
            self.user = JSON.parse(res.user);
            self.addEnrollmentEmail(self.user);
          } else {
            self.removeEnrollmentEmail(Number(res.userremoveid));
          }
          self.updateCostView();
        },
      });
    },

    addEnrollmentEmail: function ({id, email, firstname, lastname}, current = false) {
      var self = this;
      var buttonId = `button-enrollment-remove-${id}`;
      var tag = `
      <small class="tag ml-2 px-3 py-1 bg-gray text-warning d-inline-block rounded-pill"></small>
      `;
      var buttonRemove = `
      <button
      role="button"
      id="${buttonId}"
      enrol-id="${id}"
      enrol-email="${email}"
      class="btn btn-link enable-mr p-0"
      title="${self.mdlstr["removearegistrant"]}"
      disabled
      >
        <i style="padding: 0.2rem 0.7rem;" class="icon fa fa-trash mr-0" aria-hidden="true"></i>
        <!-- Remove -->
      </button>
      `;
      var tr = `
      <tr id="tr-enroll-${id}-user" style="color: #a3a3a3;">
        <td style="vertical-align: inherit; text-align: left; padding-left: 1em;" >
        <span style="display: block">
          ${firstname} ${lastname}
          ${current ? tag : ""}
        </span>
        <strong>${email}</strong>
        </td>
        <td style="vertical-align: inherit;" >
          ${buttonRemove}
        </td>
      </tr>
      `;
      self.formEnrollment.removeClass("was-validated");
      self.tbodyEnrollmentList.append(tr);
      self.trEnrollmentUser.removeClass("d-none");
      self.formEnrollment
        .children(".form-row")
        .children(".form-group")
        .children("input")
        .prop("value", "");
      var removeId = `button#${buttonId}`;
      var btn = $(removeId);
      var email = btn.attr("enrol-email");

      btn.prop("disabled", false).click(function () {
        self.checkEnrollmentEmail(email, 2);
      });
    },

    removeEnrollmentEmail: function (id) {
      var self = this;
      $(`tr#tr-enroll-${id}-user`).remove();
      self.statusPay();
    },

    errorFrom: function (message, name = "trDiscountError") {
      const error = this[name];
      if (!error) {
        return;
      }
      error.removeClass("d-none");
      error.children("td").children(".alert-danger").text(message);
    },

    updateCostView: function () {
      var self = this;
      let discounttotal = Number(self.discounttotal);
      let threshold = Number(self.threshold);
      let codegiven = Boolean(self.codegiven);
      let userids = self.userids.split(",");
      let visibleDiscount = (self.spanThreshold && threshold) || discounttotal;
      const Float = (value, ext = "") =>
        `${parseFloat(value).toFixed(2)}${ext}`;

      // refres UI data
      var updateFields = (name = "formStripe") => {
        const form = self[name];
        if (!form) {
          return;
        }
        if (form?.children("input")?.length) {
          const keys = Object.keys(self);
          for (const key of keys) {
            const el = form.children(`input[name='${key}']`);
            if (el) {
              el.val(self[key]);
            }
          }
        }
      };

      if (self.directenrollment) {
        self.trBuyNowAction.removeClass("d-none");
        self.spanBankTransferInfo.addClass("d-none");
        self.trGatewayAction.addClass("d-none");
        self.pPaypalInfo.addClass("d-none");
      } else {
        self.spanBankTransferInfo.removeClass("d-none");
        self.trGatewayAction.removeClass("d-none");
        self.pPaypalInfo.removeClass("d-none");
        self.trBuyNowAction.addClass("d-none");
      }

      if (self.spanThreshold && threshold) {
        self.spanWillApply.addClass("d-none");
        self.spanDiscountApply.addClass("d-none");
        self.spanThreshold.text(threshold);

        if (userids?.length >= threshold && codegiven) {
          self.spanDiscountApply.removeClass("d-none");
        } else {
          self.spanWillApply.removeClass("d-none");
        }
      }

      if (self.taxsubtotal) {
        self.spanTaxSubtotal.text(Float(self.taxsubtotal, " "));
      }
      if (self.taxamount) {
        self.spanTaxAmount.text(Float(self.taxamount, " "));
      } else {
        self.spanTaxAmount.text(Float(self.taxamount, " "));
      }

      if (discounttotal || self.directenrollment) {
        self.trDiscount.removeClass("d-none");
        self.spanDiscountsubtotal.text(Float(self.discountsubtotal, " "));

        if (self.discountisvalue) {
          self.spanDiscountamount.text(Float(self.discountamount, " "));
        } else {
          self.spanDiscountamount.text(Float(self.discountamount, " "));
        }
      }

      if (self.units > 1) {
        self.spanUnits.text(`(x${self.units})`);
        self.spanUnits.removeClass("d-none");
      } else {
        self.spanUnits.addClass("d-none");
      }

      var totalPay = self.subtotal;
      if (self.discountamount) {
        totalPay = discounttotal;
      }
      if (self.taxtotal) {
        totalPay = self.taxtotal;
      }

      self.spanTaxTotal.text(Float(totalPay));
      self.bankTransferCost.text(
        self.symbol + "" + Float(totalPay, " " + self.currency)
      );

      if (self.formStripe?.children("input")?.length) {
        updateFields("formStripe");
      }

      if (self.formPaypal?.children("input")?.length) {
        updateFields("formPaypal");
      }

      if (self.formBuynow?.children("input")?.length) {
        updateFields("formBuynow");
      }

      if (visibleDiscount) {
        // Show Alert A ${discout} per-person discount is applied
        self.spanDiscountAlert.text(
          self.discountisvalue
            ? self.symbol + self.discountamount
            : self.percentage + "%"
        );
      }

      self.statusPay();
    },

    initClickHandlers: function () {
      var self = this;

      self.formDiscountCode.submit(function (event) {
        self.formDiscountCode.addClass("was-validated");
        event.preventDefault();
        var data = self.getDataForm(self.formDiscountCode);
        var status = self.errorEmpty(
          data?.discountcode,
          "incorrectdiscountcode_desc"
        );
        if (status) {
          return self.errorFrom(status);
        }
        self.checkDiscountCode(data);
      });

      self.formEnrollment.submit(function (event) {
        event.preventDefault();
        var { email } = self.getDataForm(self.formEnrollment);
        var status = self.errorEmpty(email, "novalidemailsentered");
        if (status) {
          return self.errorFrom(status, "trEnrollmentError");
        }
        self.checkEnrollmentEmail(email, 1);
        self.formEnrollment
          .addClass("was-validated")
          .children(".form-row")
          .children(".form-group")
          .children("input[type=email]")
          .focus();
      });
    },

    statusForm: function (formName = "", disabled = false, focus = false) {
      const form = this[formName];
      if (!form) {
        return;
      }
      form.prop("method", "POST");
      form
        .children(".form-row")
        .children(".form-group")
        .children("button.btn")
        .prop("type", "submit")
        .prop("disabled", disabled);

      const input = form
        .children(".form-row")
        .children(".form-group")
        .children("input");
      if (focus) {
        input.prop("disabled", disabled).focus();
      } else {
        input.prop("disabled", disabled);
      }
    },

    statusPay: function () {
      var self = this;
      var el = $(".tbody-enrollment-list");
      var status = !(
        el?.children()?.length == undefined || el?.children()?.length <= 0
      );

      self.formBuynow
        .children("button")
        .prop("type", status ? "submit" : "button")
        .prop("disabled", !status);

      self.formStripe
        .children("button")
        .prop("type", status ? "submit" : "button")
        .prop("disabled", !status);

      self.formPaypal
        .children("button")
        .prop("type", status ? "submit" : "button")
        .prop("disabled", !status);

      if (status) {
        self.trEnrollmentUser.removeClass("d-none");
      } else {
        self.trEnrollmentUser.addClass("d-none");
      }
      self.tbodyEnrollmentList = el;
    },

    setQuery: function () {
      this.alertSuccess = $(".discount-threshold-info.alert");
      this.alertError = $(".enrol-alert-danger");
      this.discountContainer = $("#discount-container");

      this.statusForm("formEnrollment", false);
      this.statusForm("formDiscountCode", false, true);
    },

    getDataForm: function (form) {
      var inputs = {};
      var URLencoded = form.serialize();
      var sections = URLencoded.split("&");
      for (const section of sections) {
        var paramts = section.split("=");
        inputs = { ...inputs, [paramts[0]]: paramts[1] };
      }
      return inputs;
    },

    setValueOfObject: function (paramts) {
      const self = this;
      const keys = Object.keys(paramts);

      for (const key of keys) {
        self[key] = paramts[key];
      }
    },

    errorEmpty: function (val, code) {
      if (typeof val == "undefined") {
        return this.mdlstr[code];
      }
      if (!val || (val && !$.trim(val))) {
        return this.mdlstr[code];
      }
      return false;
    },

    /**
     * Process a list of enrol_payment language strings into a list index
     * by string name.
     *
     * Then in the callback, we can do e.g.
     * to write the "enrolothers" language string.
     *
     * @param  {Array}    keys
     * @param  {Function} callback
     * @return {[type]}
     */
    loadStrings: function (keys = [], callback = () => {}) {
      var strs = [];
      for (var i = 0; i < keys.length; i++) {
        strs.push({ key: keys[i], component: "enrol_payment" });
      }
      var str_promise = MoodleStrings.get_strings(strs);
      str_promise.done(function (strs) {
        var ret = [];
        for (var i = 0; i < keys.length; i++) {
          ret[keys[i]] = strs[i];
        }
        callback(ret);
      });
    },

    /**
     * Start javascript module with the parameters sent from main.
     *
     * @param  {[type]} params
     * @return {[type]}
     */
    init: function (params) {
      const self = this;
      const props = JSON.parse(params);
      props.stringkeys = JSON.parse(props.stringkeys);
      self.setValueOfObject(props);

      const userJson = self.dateRef.attr("data-user");
      const user = JSON.parse(userJson);

      const costJson = self.dateRef.attr("data-cost");
      const cost = JSON.parse(costJson);
      self.setValueOfObject(cost);

      self.loadStrings(self.stringkeys, function (strs) {
        self.mdlstr = strs;
        self.setQuery();
        self.initClickHandlers();

        Promise.resolve().then(() => {
            if (user) {
                return self.addEnrollmentEmail(user, true);
            }
        }).then(() => {
            self.updateCostView();
        }).catch((error) => {
            window.console.error("An error occurred:", error);
        });
      });

    },

    settings: function () {
      // Define an array of elements containing name and select properties
      var elements = [
        {
          name: "s_enrol_payment_definetaxes",
          select: "input[name='s_enrol_payment_definetaxes']",
        },
      ];

      // Initialize an empty object to store input elements
      var inputs = {};

      // Set up an interval function that runs every 800 milliseconds
      var intervar = setInterval(function () {
        // Loop through each element in the 'elements' array
        for (var element of elements) {
          // Assign jQuery-selected input elements to the 'inputs' object using their names
          inputs[element.name] = $(element.select);

          // Check if the selected input elements exist
          if (inputs[element.name].length) {
            // Iterate through each input element
            inputs[element.name].each(function () { // eslint-disable-line no-loop-func
              var el = $(this);

              // Check if the element exists and has a 'name' property
              if (el.length && el.prop("name")) {
                var checked = el.prop("checked");

                // Call the 'disabled' function with the opposite value of 'checked'
                disabled(!checked); // eslint-disable-line no-loop-func

                // Attach a change event handler to the element
                el.on("change", function () {
                  checked = el.prop("checked");

                  // Call the 'disabled' function with the opposite value of 'checked'
                  disabled(!checked); // eslint-disable-line no-loop-func
                });
              }
            });
          }
        }

        // Check if all input elements have been successfully selected
        if (Object.values(inputs).every((inp) => inp && inp.length)) {
          // Call the 'clear' function to clear the interval and log the 'inputs' object
          clear();
        }
      }, 800);

      // Define a function to clear the interval and log the 'inputs' object
      var clear = () => {
        clearInterval(intervar);
      };

      // Define a function to set the 'disabled' property of certain elements
      var disabled = (value = true) => {
        // Define an array of queries for specific elements
        var querys = [
          "textarea[name='s_enrol_payment_taxdefinitions']",
          "input[name='s_enrol_payment_countrytax']",
        ];

        // Loop through the 'querys' array and set the 'disabled' property of matching elements
        for (const query of querys) {
          $(query).prop("disabled", value);
        }
      };
    },
  };

  return EnrolPage;
});
