<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is a one-line short description of the file.
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package   enrol_payment
 * @copyright 2020 LMS Doctor <andres.ramos@lmsdoctor.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_payment;

use moodle_exception;
use moodle_url;
use stdClass;

/**
 * Forum subscription manager.
 *
 * @package   enrol_payment
 * @copyright 2020 Andrés Ramos, LMS Doctor <andres.ramos@lmsdoctor.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class helper {

    protected $istaxable = false;
    protected $usertax = 0;

    /**
     * Constructor.
     *
     * @param string $instance
     * @param array  $config
     */
    public function __construct() {
    }

    /**
     * Get the payment from token.
     *
     * @param  string $prepaytoken
     * @return stdClass
     */
    public static function get_payment_from_token(string $prepaytoken) {
        global $DB;
        $getdata = $DB->get_record_sql(
            'SELECT * FROM {enrol_payment_session} WHERE '
                . $DB->sql_compare_text('prepaytoken') . ' = ? ',
            ['prepaytoken' => $prepaytoken]
        );

        return self::formatproperty($getdata);
    }

    /**
     * Response error coupon.
     *
     * @param  string $keytranslate
     * @param  string $code
     * @param  bool $error
     * @return string
     */
    public static function response_externallib(string $keytranslate, string $code, bool $error = true) {
        return json_encode([
            'error' => $error,
            'code' => $code,
            'errormsg' => get_string($keytranslate, 'enrol_payment')
        ]);
    }

    /**
     * Normalize percent discount.
     *
     * @param  stdClass $instance
     * @return int
     */
    public static function normalize_percent_discount(float $amount, $discounttype) {
        if ($amount > 1 && $discounttype == 1) {
            return $amount * 0.01;
        }
        return $amount;
    }

    /**
     * Normalize percent discount.
     *
     * @param  $coupon
     * @param  int $units
     * @param  bool $error
     * @return bool | string
     */
    public static function validate_coupon($coupon, int $courseid, bool $error = false, bool $usagelimit = false) {
        // Valid coupon.
        if (empty($coupon) || !is_object($coupon)) {
            if ($error) {
                return self::response_externallib('incorrectdiscountcode_desc', 'coupon is null');
            }
            return false;
        }

        // Valid coupon type.
        if (property_exists($coupon, 'type') && intval($coupon->type) <= 0) {
            if ($error) {
                return self::response_externallib('incorrectdiscountcode_desc', 'type is null');
            }
            return false;
        }

        // Valid coupon course.
        if (property_exists($coupon, 'course') && $coupon->course && $courseid) {
            $iscourse = strpos((string) $coupon->course, (string) $courseid);
            if ($iscourse === false) {
                if ($error) {
                    return self::response_externallib('incorrectdiscountcode_desc', 'course is null');
                }
                return false;
            }
        }

        // Valid usage limit.
        if (property_exists($coupon, 'usagelimit') && intval($coupon->usagelimit) > 0) {
            $limit = intval($coupon->usagelimit);
            $uses = intval($coupon->uses) + 1;
            if ($uses > $limit) {
                if ($error) {
                    return self::response_externallib('usagelimitdiscountcode_desc', "usage limit $limit");
                }
                return false;
            }
        }

        return true;
    }

    /**
     * Calculate cost.
     *
     * @param  stdClass $instance
     * @param  stdClass $paymentsession
     * @return stdClass
     */
    public static function formatproperty(stdClass $order) {
        $order->id = (int)$order->id;
        $order->userid = (int)$order->userid;
        $order->courseid = (int)$order->courseid;
        $order->instanceid = (int)$order->instanceid;
        $order->multiple = (int)$order->multiple;
        $order->units = (int)$order->units;
        $order->originalcost = (float)$order->originalcost;
        $order->taxpercent = (float)$order->taxpercent;
        $order->status = (int)$order->status;

        return $order;
    }

    /**
     * Calculate cost.
     *
     * @param  stdClass $coupon
     * @param  stdClass $paymentsession
     * @return stdClass
     */
    public static function calculate_cost(stdClass $coupon, stdClass $paymentsession) {
        if (self::not_enough_units($paymentsession->units)) {
            throw new moodle_exception('notenoughunits', 'enrol_payment');
        }

        $counter = self::initialize_counter($coupon, $paymentsession);

        $thresholdwasapplied = self::threshold_applies($counter);
        $counter->percentage = self::calculate_percentage($coupon, $counter);

        if ($thresholdwasapplied && !$counter->discountisvalue) {
            self::discount_is_overlimit($counter->percentage);
        }

        self::apply_discounts($counter, $thresholdwasapplied);

        if ($counter->hastax) {
            self::apply_taxes($counter, $thresholdwasapplied);
        }

        return $counter;
    }

    private static function initialize_counter($coupon, $paymentsession) {
        $type = property_exists($coupon, "type") ? (int) $coupon->type : 0;
        $amount = property_exists($coupon, "amount") ? (float) $coupon->amount : 0.00;
        $threshold = (int) $coupon->threshold ?? 1;
        $units = (int) $paymentsession->units ?? 1;
        $price = (float) $paymentsession->originalcost ?? 0.00;
        $tax = (float) $paymentsession->taxpercent ?? 0.00;
        $subtotal = self::convert_to_float($price * $units);

        $counter = new stdClass;
        $counter->discountamount = $amount;
        $counter->discounttype = $type;
        $counter->discountisvalue = $type == 2;
        $counter->codegiven = (bool) $paymentsession->codegiven ?? 0;
        $counter->userids = (string) $paymentsession->multipleuserids;
        $counter->units = $units;
        $counter->price = self::convert_to_float($price);
        $counter->subtotal = self::convert_to_float($subtotal);
        $counter->threshold = $threshold;
        $counter->coderequired = 1;
        $counter->tax = self::convert_to_float($tax * 100);
        $counter->hastax = self::ispositive($tax);

        return $counter;
    }

    private static function calculate_percentage($coupon, $counter) {
        return $counter->discountisvalue ? (($counter->discountamount / $counter->price) * 100) : $counter->discountamount;
    }

    private static function apply_discounts(&$counter, $thresholdwasapplied) {
        if ($thresholdwasapplied) {
            $caldiscount = self::apply_percentage($counter->percentage, $counter->price, false);
            $counter->discountamount = self::convert_to_float($caldiscount->amount);
            $counter->discountsubtotal = self::convert_to_float($caldiscount->total);
            $counter->discounttotal = self::convert_to_percentage($caldiscount->total * $counter->units);
        } else {
            $counter->discountsubtotal = 0.00;
            $counter->discounttotal = 0.00;
        }
    }

    private static function apply_taxes(&$counter, $thresholdwasapplied) {
        $counter->taxamount = 0.00;
        $counter->taxsubtotal = 0.00;
        $counter->taxtotal = 0.00;

        if ($counter->tax > 0) {
            $price = $thresholdwasapplied ? $counter->discountsubtotal : $counter->price;
            $taxpercentage = self::convert_to_percentage($counter->tax);
            $caltax = self::apply_percentage($taxpercentage, $price);

            $counter->taxamount = self::convert_to_float($caltax->amount);
            $counter->taxsubtotal = self::convert_to_float($caltax->total);
            $counter->taxtotal = self::convert_to_percentage($caltax->total * $counter->units);
        }
    }

    /**
     * Validate if the threshold applies.
     *
     * @param  stdClass $instance
     * @return bool
     */
    private static function threshold_applies(stdClass $instance) {
        return ($instance->units >= $instance->threshold && $instance->codegiven);
    }

    /**
     * Validate if the threshold applies.
     *
     * @param  stdClass $instance
     * @return null|stdClass
     */
    public static function convert_to_courses(stdClass $data, bool $tostring = true) {

        if (!$data) {
            return null;
        }

        $data->code = trim($data->code);
        $ids = $data->course;

        if (!$ids) {
            return $data;
        }

        if ($tostring) {
            $data->course = implode(",", $ids);
        } else {
            $data->course = explode(',', $ids);
        }

        return $data;
    }

    /**
     * @param  float|int $percentage
     * @param  float $amount
     * @param  bool $ispercentage
     * @return float
     */
    public static function convert_to_percentage($percentage, float $amount = 0, bool $ispercentage = true) {
        if (!self::ispositive($percentage)) {
            return 0.00;
        }
        if ($ispercentage) {
            return self::convert_to_float($percentage);
        }
        if ($amount) {
            return ((($percentage / $amount)) * 100) * 0.01;
        }
        return self::convert_to_float($percentage * 0.01);
    }

    /**
     * @param  float|int $percentage
     * @param  float|int $value
     * @param  bool $sum
     * @return stdClass
     */
    public static function apply_percentage($percentage, $value, bool $sum = true) {
        $apply = new stdClass;
        $apply->value = self::convert_to_float($value);
        $apply->percentage = $percentage;
        $apply->amount = (($percentage / 100) * $apply->value);
        $apply->total = self::convert_to_float($apply->amount + $apply->value);

        if ($apply->amount > 0 && !$sum) {
            $apply->total = self::convert_to_float($apply->value - $apply->amount);
            if ($apply->total < 0) {
                $apply->total = 0.00;
            }
        }
        return $apply;
    }

    /**
     * @param  int $value
     */
    public static function convert_to_float($value) {
        if (empty($value) || !$value) {
            return 0.00;
        }
        return number_format($value, 2, ".", '');
    }

    /**
     * @param $value
     */
    public static function ispositive($value) {
        if (empty($value) || !$value) {
            return false;
        }
        return true;
    }

    /**
     * @param  stdClass|array $value
     * @param  bool $die
     * @param  bool $json
     */
    public static function print_var_drom($value, bool $die = true, $json = false) {
        sleep(1);
        $id = strtotime(date("Y-m-d H:i:s"), time());
        echo ("<pre id='$id'><code >");
        var_dump($value);
        if ($json) {
            echo ("
            <script>
                var data = JSON.stringify(" . json_encode((array) $value) . ", null, 3);
                document.getElementById('$id').innerHTML = '<code>'+data+'</code>';
            </script>
        ");
        }
        echo ("</code></pre>");
        if ($die) {
            die();
        }
    }

    /**
     * Check if the discount is enabled.
     *
     * @param  int  $discounttype
     *
     * @return bool
     */
    public static function has_discount($discounttype) {
        return (get_config('enrol_payment', 'enablediscounts') && $discounttype != 0);
    }

    /**
     * Check if the discount is overlimit.
     *
     * @param  float $discount
     * @return void
     */
    private static function discount_is_overlimit($discount) {
        if ($discount > 100) {
            throw new \Exception(get_string("percentdiscountover100error", "enrol_payment"));
        }
    }

    /**
     *
     */
    public static function get_string_keys() {
        return [
            "discounttypeerror",
            "discountamounterror",
            "invalidgateway",
            "errcommunicating",
            "addaregistrant",
            "removearegistrant",
            "enrolothers",
            "cancelenrolothers",
            "confirmpurchase",
            "continue",
            "dismiss",
            "invalidpaymentprovider",
            "novalidemailsentered",
            "novalidemailsentered_desc",
            "totalenrolmentfee",
            "error",
            "incorrectdiscountcode",
            "incorrectdiscountcode_desc",
            "registrant",
            "enteremail",
            "couponsuccessfullyapplied",
            "thisuserdoesnotexist",
            "discountwillapply",
        ];
    }

    /**
     * Returns paypal url
     *
     * @return string
     */
    public static function paypal_url() {
        $sandbox = get_config('enrol_payment', 'enablesandbox');
        $env = (bool)$sandbox ? '.sandbox.paypal.com' : '.paypal.com';
        return 'https://www' . $env . '/cgi-bin/webscr';
    }

    /**
     * Returns inputs of pay
     *
     * @param  stdClass $user
     * @param  stdClass $fields
     * @param  stdClass $cost
     * @return string
     */
    public static function standard_input(stdClass $cost) {
        return "
        <!-- data config -->
        <input type='hidden' name='sessionid'          value='$cost->sessionid'/>
        <input type='hidden' name='courseid'          value='$cost->courseid'/>
        <input type='hidden' name='instanceid'        value='$cost->instanceid'/>
        <input type='hidden' name='currency'          value='$cost->currency'/>
        <input type='hidden' name='prepaytoken'       value='$cost->prepaytoken'/>
        <input type='hidden' name='coursename'        value='$cost->coursename'/>
        <!-- data cost -->
        <input type='hidden' name='price'             value='$cost->price'/>
        <input type='hidden' name='subtotal'          value='$cost->subtotal'/>
        <input type='hidden' name='discountamount'    value='$cost->discountamount'/>
        <input type='hidden' name='discountsubtotal'  value='$cost->discountsubtotal'/>
        <input type='hidden' name='discounttotal'     value='$cost->discounttotal'/>
        <input type='hidden' name='taxamount'         value='$cost->taxamount'/>
        <input type='hidden' name='taxsubtotal'       value='$cost->taxsubtotal'/>
        <input type='hidden' name='taxtotal'          value='$cost->taxtotal'/>
        <input type='hidden' name='userids'           value='$cost->userids'/>
        <input type='hidden' name='units'             value='1'/>
        ";
    }

    /**
     * Returns and object holding the cost values.
     *
     * @param  array  $ret
     * @param  string $symbol
     * @param  string $currency
     * @param  int    $units
     * @param  string $taxstring
     * @return [type]
     */
    public static function get_object_of_costs(array $ret, string $symbol, string $currency, int $units, string $taxstring) {
        $data = new stdClass;
        $data->symbol = $symbol;
        $data->originalcost = $ret['unitprice'];
        $data->unitdiscount = $ret['percentdiscountunit'];
        $data->percentdiscount = $ret['percentdiscount'];
        $data->units = $units;
        $data->subtotaltaxed = $ret['subtotaltaxed'];
        $data->taxstring = $taxstring;
        $data->currency = $currency;
        $data->discountvalue = $ret['discountvalue'];
        return $data;
    }

    /**
     * Check if units are 0.
     *
     * @param  int    $units
     * @throws moodle_exception
     */
    public static function not_enough_units(int $units) {
        return empty($units);
    }

    /**
     * Returns the percentage calculation string.
     *
     * @param  stdClass $obj
     * @return string
     */
    public static function get_percentage_calculation_string(stdClass $obj, bool $codegiven, bool $hasdiscount) {
        if (!$hasdiscount) {
            return get_string('percentcalculation', 'enrol_payment', $obj);
        }
        return get_string('percentcalculationdiscount', 'enrol_payment', $obj);
    }

    /**
     * Returns the percentage discount string.
     *
     * @param  stdClass $obj
     * @return string
     */
    public static function get_percentage_discount_string(stdClass $obj, bool $codegiven, bool $hasdiscount) {
        if (!$hasdiscount) {
            return '';
        }
        return get_string('percentdiscountstring', 'enrol_payment', $obj);
    }

    /**
     * When switching between Single and Multiple mode, make the necessary
     * adjustments to our payment row in the database.
     *
     * @param  bool $multiple
     * @param  array $users
     * @param  stdClass &$payment
     * @return void
     */
    public static function update_payment_data(bool $multiple, array $users, stdClass &$payment) {
        global $DB;

        $userids = array();
        if ($multiple) {
            foreach ($users as $u) {
                array_push($userids, $u["id"]);
            }
        }

        $payment->multiple = $multiple;
        $payment->multipleuserids = $multiple ? implode(",", $userids) : null;
        $payment->units = $multiple ? count($userids) : 1;
        $DB->update_record('enrol_payment_session', $payment);
    }

    /**
     * Return true if the corresponding transaction is pending.
     *
     * @param  int $paymentid
     * @return bool
     */
    public static function payment_pending(int $paymentid) {
        global $DB;
        $payment = $DB->get_record('enrol_payment_session', array('id' => $paymentid));
        $transaction = $DB->get_record('enrol_payment_transaction', array('txnid' => $payment->paypaltxnid));

        if ($transaction) {
            return ($transaction->payment_status == "Pending");
        }

        return false;
    }

    /**
     * Returns the payment status.
     *
     * @param  int $paymentid
     * @return string
     */
    public static function get_payment_status(int $paymentid) {
        global $DB;
        $payment = $DB->get_record('enrol_payment_session', array('id' => $paymentid));
        $transaction = $DB->get_record('enrol_payment_transaction', array('txnid' => $payment->paypaltxnid));

        if ($transaction) {
            return $transaction->paymentstatus;
        }
        return null;
    }

    /**
     * Look for users using email addresses.
     *
     * @param  array $emails
     * @return array
     */
    public static function get_moodle_users_by_emails(array $emails) {
        global $DB;

        $users = [
            'found' => [],
            'notfound' => [],
        ];
        foreach ($emails as $email) {
            // Make sure to remove any extra spaces.
            $email = trim($email);
            $user = $DB->get_record('user', ['email' => $email], 'id, email, firstname, lastname');
            if ($user) {
                $userdata = [
                    'id' => $user->id,
                    'email' => $email,
                    'name' => ($user->firstname . " " . $user->lastname)
                ];
                array_push($users['found'], $userdata);
            } else {
                array_push($users['notfound'], $email);
            }
        }

        return $users;
    }

    /**
     * Pretty print user.
     *
     * @param  array $user
     * @return string
     */
    public static function pretty_print_user(array $user) {
        return $user['name'] . " &lt;" . $user['email'] . "&gt;";
    }

    /**
     * Persistent payment session
     *
     * @param  stdClass params
     * @example persistent_payment_session((object)[
     *  "token" => (string),
     *  "courseid" => (int),
     *  "enrolid" => (int),
     *  "price" => (float),
     *  "tax" => (float)
     * ])
     * @return stdClass
     */
    public static function persistent_payment_session(stdClass $params) {
        global $DB;
        $table = 'enrol_payment_session';
        $refskey = (object)["course" => $params->courseid, "enrol" => $params->enrolid];
        $payment = (object)["price" => $params->price, "tax" => $params->tax];
        $data = (object) self::get_payment_object($params->token, $refskey, $payment);

        $session = $DB->get_record($table, array(
            'courseid' => $data->courseid,
            'userid' => $data->userid,
            'status' => $data->status
        ));

        if (empty($session)) {
            if ($id = $DB->insert_record($table, $data)) {
                return $DB->get_record($table, array('id' => $id));
            }
        }

        $data->id = $session->id;
        $DB->update_record('enrol_payment_session', $data);
        return $DB->get_record($table, array('id' => $data->id));
    }

    /**
     * Persistent payment session
     *
     * @param  stdClass refskey
     * @param  stdClass payment
     * @example get_payment_object(
     * (object)[
     *  "course" => (int),
     *  "enrol" => (in)
     * ])
     * (object)[
     *  "price" => (float),
     *  "tax" => (float)
     * ])
     * @return array
     */
    public static function get_payment_object(string $prepaytoken, stdClass $refskey, stdClass $payment) {
        global $USER;
        return [
            'prepaytoken' => $prepaytoken,
            'userid' => $USER->id,
            'courseid' => $refskey->course,
            'instanceid' => $refskey->enrol,
            'multiple' => true,
            'multipleuserids' => $USER->id,
            'codegiven' => false,
            'units' => 1,
            'originalcost' => $payment->price,
            'taxpercent' => $payment->tax,
            'paypaltxnid' => null,
            'coupon' => null,
            'status' => 0,
        ];
    }

    /**
     * Returns the stripe logo url.
     *
     * @return string
     */
    public static function get_stripe_logo_url() {
        $stripelogo = get_config('enrol_payment', 'stripelogourl');
        if (empty($stripelogo)) {
            return '';
        }
        $strippedlogo = str_replace('/', '', $stripelogo);
        return (string) moodle_url::make_pluginfile_url(1, 'enrol_payment', 'stripelogo', null, '/', $strippedlogo);
    }

    /**
     * Alerts site admin of potential problems.
     *
     * @param string $message
     */
    public static function message_paypal_error_to_admin(string $message) {
        global $CFG;

        $admin = get_admin();
        $site = get_site();

        $subject = get_string('transactionfailed', 'enrol_payment');
        $msg = "$site->fullname: \n\n$message\n\n";
        $messagehtml = '<p>' . $msg . '</p>';
        $messagetext = html_to_text($messagehtml);
        $fromuser = self::get_fromuser_object();

        return email_to_user($admin, $fromuser, $subject, $messagetext, $messagehtml, ", ", false);
    }

    /**
     * Returns fromuser object to be used in email_to_user function.
     *
     * @return stdClass
     */
    public static function get_fromuser_object() {
        global $CFG;

        $fromuser = new stdClass;
        $fromuser->email = $CFG->supportemail;
        $fromuser->firstname = $CFG->supportname;
        $fromuser->lastname = '';
        $fromuser->mailformat = 1;
        $fromuser->id = -99;
        $fromuser->firstnamephonetic = '';
        $fromuser->lastnamephonetic = '';
        $fromuser->middlename = '';
        $fromuser->alternatename = '';

        return $fromuser;
    }

    /**
     * Silent exception handler.
     *
     * @return callable exception handler
     */
    public static function get_exception_handler() {
        return function ($ex) {
            $info = get_exception_info($ex);

            $logerrmsg = "enrol_payment IPN exception handler: " . $info->message;
            if (debugging('', DEBUG_NORMAL)) {
                $logerrmsg .= ' Debug: ' . $info->debuginfo . "\n" . format_backtrace($info->backtrace, true);
            }
            echo $logerrmsg;

            if (http_response_code() == 200) {
                http_response_code(500);
            }

            exit(0);
        };
    }

    /**
     * Outputs transfer instructions.
     *
     * @param  float $cost
     * @param  string $coursefullname
     * @param  string $courseshortname
     * @return string
     */
    public static function get_transfer_instructions(float $cost, string $coursefullname, string $courseshortname) {

        if (!get_config('enrol_payment', 'allowbanktransfer')) {
            return '';
        }

        $instructions = get_config('enrol_payment', 'transferinstructions');
        $instructions = str_replace("{{AMOUNT}}", "<strong id=\"banktransfer-cost\">$cost</strong>", $instructions);
        $instructions = str_replace("{{COURSESHORTNAME}}", $courseshortname, $instructions);
        $instructions = str_replace("{{COURSEFULLNAME}}", $coursefullname, $instructions);

        return $instructions;
    }

    /**
     * Enrollment notification messages
     *
     * @param  \enrol_plugin $plugin
     * @param  mixed $course
     * @param  mixed $user
     * @return void
     */
    public static function notification_messages(\enrol_plugin $plugin, $course, $user) {
        global $CFG;

        $context = \context_course::instance($course->id, MUST_EXIST);
        $coursecontext = \context_course::instance($course->id, IGNORE_MISSING);

        // Pass $view=true to filter hidden caps if the user cannot see them
        $teacher = false;
        if ($users = get_users_by_capability(
            $context,
            'moodle/course:update',
            'u.*',
            'u.id ASC',
            '',
            '',
            '',
            '',
            false,
            true
        )) {
            $users = sort_by_roleassignment_authority($users, $context);
            $teacher = array_shift($users);
        }

        $mailstudents = $plugin->get_config('mailstudents');
        $mailteachers = $plugin->get_config('mailteachers');
        $mailadmins   = $plugin->get_config('mailadmins');

        $a = new stdClass();
        $a->course = format_string($course->fullname, true, array('context' => $coursecontext));
        $a->user = fullname($user);
        $a->email = $user->email;
        $a->profileurl = "$CFG->wwwroot/user/view.php?id=$user->id&course=$course->id";

        $shortname = format_string($course->shortname, true, array('context' => $context));
        $welcomemessage = (!empty($plugin->welcomemessage)) ? $plugin->welcomemessage : $plugin->get_config('defaultcoursewelcomemessage');

        if (empty(trim($welcomemessage))) {
            $welcomemessage = get_string('welcometocoursetext', '', $a);
        }

        // The student gets the welcome custom message either from the instance or plugin settings.
        if (!empty($mailstudents)) {
            // If a custom message is set.
            $key = array('{COURSENAME}', '{PROFILEURL}', '{FULLNAME}', '{EMAIL}');
            $value = array($a->course, $a->profileurl, fullname($user), $user->email);
            $message = str_replace($key, $value, $welcomemessage);
            $userfrom = empty($teacher) ? \core_user::get_noreply_user() : $teacher;
            self::notification_messages_send($course->id, $userfrom, $user, $shortname, $message);
        }

        $message = get_string('enrolmentnewuser', 'enrol', $a);
        if (!empty($mailteachers) && !empty($teacher)) {
            self::notification_messages_send($course->id, $user, $teacher, $shortname, $message);
        }

        if (!empty($mailadmins)) {
            $admins = get_admins();
            foreach ($admins as $admin) {
                self::notification_messages_send($course->id, $user, $admin, $shortname, $message);
            }
        }
    }

    /**
     * Enrollment notification messages
     *
     * @param  int $courseid
     * @param  mixed $userfrom
     * @param  mixed $userto
     * @param  string $shortname
     * @param  mixed $a
     * @return void
     */
    private static function notification_messages_send($courseid, $userfrom, $userto, $shortname, $message) {
        $eventdata = new \core\message\message();
        $eventdata->courseid          = $courseid;
        $eventdata->modulename        = 'moodle';
        $eventdata->component         = 'enrol_payment';
        $eventdata->name              = 'payment_enrolment';
        $eventdata->userfrom          = $userfrom;
        $eventdata->userto            = $userto;
        $eventdata->subject           = get_string('enrolmentnew', 'enrol', $shortname);
        $eventdata->fullmessage       = $message;
        $eventdata->fullmessageformat = FORMAT_HTML;
        $eventdata->fullmessagehtml   = '';
        $eventdata->smallmessage      = '';
        message_send($eventdata);
    }

    /**
     * Alerts site admin of potential problems.
     *
     * @param string   $subject email subject
     * @param stdClass $transaction
     */
    public static function notification_messages_error_to_admin($subject, $data) {
        $admin = get_admin();
        $site = get_site();

        $message = "$site->fullname:  Transaction failed.\n\n$subject\n\n";

        foreach ($data as $key => $value) {
            $message .= "$key => $value\n";
        }

        $eventdata = new \core\message\message();
        $eventdata->courseid          = empty($data->courseid) ? SITEID : $data->courseid;
        $eventdata->modulename        = 'moodle';
        $eventdata->component         = 'enrol_payment';
        $eventdata->name              = 'payment_enrolment';
        $eventdata->userfrom          = $admin;
        $eventdata->userto            = $admin;
        $eventdata->subject           = 'PAYPAL ERROR: ' . $subject;
        $eventdata->fullmessage       = $message;
        $eventdata->fullmessageformat = FORMAT_PLAIN;
        $eventdata->fullmessagehtml   = '';
        $eventdata->smallmessage      = '';
        message_send($eventdata);
    }
}
