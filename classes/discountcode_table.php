<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Discount table file.
 *
 * @package    enrol_payment
 * @copyright  2023 LMS Doctor <support@lmsdoctor.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_payment;

use moodle_url;
use pix_icon;
use html_writer;

/**
 * Overall table class.
 */
class discountcode_table extends \table_sql {

    /**
     * Constructor
     * @param int $uniqueid all tables have to have a unique id, this is used
     *      as a key when storing table properties like sort order in the session.
     */
    public function __construct($uniqueid) {
        parent::__construct($uniqueid);

        // Define the list of columns to show.
        $columns = array(
            'id',
            'code',
            'amount',
            'course',
            'threshold',
            'usagelimit',
            'uses',
        );

        // Display column if not downloading.
        if (!$this->is_downloading()) {
            $columns[] = 'actions';
        }

        $this->define_columns($columns);

        // Define the titles of columns to show in header.
        $headers = array(
            'Id',
            get_string('code', 'enrol_payment'),
            get_string('amount', 'enrol_payment'),
            get_string('courses'),
            get_string('threshold', 'enrol_payment'),
            get_string('usagelimit', 'enrol_payment'),
            get_string('uses', 'enrol_payment'),
        );

        if (!$this->is_downloading()) {
            $headers[] = 'Actions';
        }
        $this->define_headers($headers);

        // No sort columns.
        $this->no_sorting('actions');
    }

    /**
     * Returns the discount code type.
     *
     * @param stdClass $row Contains object with all the values of record.
     * @return string
     */
    public function col_type($row) {
        $options = array(
            0 => get_string('nodiscount', 'enrol_payment'),
            1 => get_string('percentdiscount', 'enrol_payment'),
            2 => get_string('valuediscount', 'enrol_payment'),
        );
        return $options[$row->type];
    }

    /**
     * Returns the amount column.
     *
     * @param stdClass $row Contains object with all the values of record.
     * @return string
     */
    public function col_amount($row) {
        $amount = $row->amount;

        $options = array(
            1 => (int) $amount . '%',
            2 => '$' . $amount,
        );

        if (!array_key_exists((int)$row->type, $options)) {
            return '0';
        }

        return $options[$row->type];
    }

    /**
     * Returns the course names o name.
     *
     * @param stdClass $row Contains string whit course id.
     * @return string
     */
    public function col_course($row) {
        global $DB;
        $course = $row->course;

        if (empty($course)) {
            return get_string('allcourses', 'enrol_payment');
        }

        $ids = explode(',', $course);
        if (is_array($ids)) {
            $courses = $DB->get_fieldset_sql("SELECT shortname from {course} WHERE id IN ($course)");
            return  implode(', ', $courses);
        }

        return $DB->get_field('course', 'shortname', array('id' => $row->course));
    }

    /**
     * Returns the course names o name.
     *
     * @param stdClass $row Contains string whit course id.
     * @return string
     */
    public function col_usagelimit($row) {
        $usagelimit = $row->usagelimit;

        if ($usagelimit == 0 || empty($usagelimit)) {
            return '-';
        }

        return $usagelimit;
    }

    /**
     * Processing of the actions value.
     *
     * @param  stdClass $row
     * @return string
     */
    public function col_actions($row) {
        global $OUTPUT;

        $disablestr = get_string('disablediscountcode', 'enrol_payment');
        $enablestr = get_string('enablediscountcode', 'enrol_payment');
        $editcoupon = get_string('editcoupon', 'enrol_payment');
        $attr = array('class' => 'action-delete');

        if (!$this->is_downloading()) {

            $visibility = (!$row->disabled) ? new pix_icon('i/hide', $disablestr) : new pix_icon('i/show', $enablestr);

            $url = 'action.php';
            $updateurl = new moodle_url($url, array('id' => $row->id, 'action' => 'edit'));
            $deleteurl = new moodle_url($url, array('id' => $row->id, 'action' => 'disable'));

            // If the discount code is disable, have action to enabled it.
            if ($row->disabled) {
                $deleteurl = new moodle_url($url, array('id' => $row->id, 'action' => 'enable'));
            }

            $actions = $OUTPUT->action_icon($updateurl, new pix_icon('i/edit', $editcoupon));
            $actions .= $OUTPUT->action_icon($deleteurl, $visibility, null, $attr);
            return $actions;
        }
    }
}
