<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main render class.
 *
 * @package   enrol_payment
 * @copyright 2023 LMS Doctor <helpdesk@lmsdoctor.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_payment\output;

defined('MOODLE_INTERNAL') || die();

use renderable;
use renderer_base;
use templatable;
use enrol_payment\helper;
use stdClass;
use moodle_url;
use Exception;
use html_writer;

require_once($CFG->dirroot . '/lib/enrollib.php');

/**
 * Renderable main class.
 *
 * @package   enrol_payment
 * @copyright 2020 LMS Doctor
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class main implements renderable, templatable {

    private $instance;

    /**
     * Constructor.
     *
     * @param stdClass $instance
     * @param array  $config
     */
    public function __construct(stdClass $instance) {
        $this->instance = $instance;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param \renderer_base $output
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        global $CFG, $USER, $PAGE, $DB, $OUTPUT;

        $this->check_user_access();
        $this->load_user_profile();

        if ($this->allow_payment_enrollment()) {
            return false;
        }

        $config = $this->get_settings();

        // No cost, other enrolment methods (instances) should be used.
        if (abs((float) $this->instance->cost) < 0.01) {
            echo html_writer::tag('p', get_string('nocost', 'enrol_payment'));
            die();
        }

        $user = $this->prepare_user_data();

        // Param of the instance.
        $requireshipping = $this->instance->customint4;
        $instanceid = $this->instance->id;
        $currency = $this->instance->currency;
        $courseid = $this->instance->courseid;

        // Check the user enrollment.
        $isenrolled = is_enrolled(\context_course::instance($courseid), $USER);

        // If don't allow new enrollment and existing enrollment.
        if (!$config->allownewenrol) {
            return array('disableenrollment' => true);
        }

        // Force login only for guest user, not real users with guest role
        // Used to verify payment data so that it can't be spoofed.
        $token = bin2hex(random_bytes(16));
        $price = helper::convert_to_float((float) $this->instance->cost);
        $tax = $config->taxinfo->taxpercent;
        $course = $DB->get_record('course', array('id' => $courseid));
        // Are discounts enabled in the admin settings?
        $symbol = enrol_payment_get_currency_symbol($currency);
        // Sanitise some fields before building the PayPal form.
        $paymentsession = helper::persistent_payment_session(
            (object)[
                'token' => $token,
                'courseid' => $course->id,
                'enrolid' => $instanceid,
                'price' => $price,
                'tax' => $tax
            ]
        );

        // Data default for test.
        $this->instance->threshold = 1;

        // Cost gateway configuration.
        $cost = helper::calculate_cost($this->instance, $paymentsession);
        $cost->symbol = $symbol;

        $cost->price =  number_format($cost->price, 2, ".", '');
        $cost->prepaytoken = $token;
        $cost->currency = $currency;
        $cost->taxstring = $config->taxinfo->taxstring;
        $cost->courseid = $course->id;
        $cost->instanceid = $instanceid;
        $cost->sessionid = $paymentsession->id;
        $cost->coursename = $course->fullname;
        $cost->courseshortname = $course->shortname;
        $cost->perseat = ' per-person';

        // Url gateway configuration.
        $paymentgateway = new stdClass;
        $paymentgateway->shipping = $requireshipping ? 2 : 1;
        $paymentgateway->paypalaccount = $config->paypalaccount;
        $paymentgateway->sessionid = $paymentsession->id;
        $paymentgateway->instanceid = $instanceid;
        // Routea gateway.
        $paymentgateway->paypalaction = helper::paypal_url();
        $paymentgateway->buynowaction = new moodle_url("/enrol/payment/gateway/buynow/transaction.php");
        $paymentgateway->stripeaction = new moodle_url("/enrol/payment/gateway/checkout.php");
        $locale = 'en_CA';
        $paymentgateway->paypalsdk = "https://www.paypal.com/sdk/js?locale=$locale&client-id=$config->paypalclientid&currency=$currency";

        if (!$config->allowcard) {
            $paymentgateway->paypalsdk = "https://www.paypal.com/sdk/js?locale=$locale&client-id=$config->paypalclientid&currency=$currency&disable-funding=credit,card";
        }

        $paymentgateway->transaction = new moodle_url("/enrol/payment/gateway/transaction.php");
        $paymentgateway->ipnurl = new moodle_url('/enrol/payment/gateway/ipn.php');
        $paymentgateway->returnurl = new moodle_url('/enrol/payment/gateway/return.php', [
            'id' => $course->id, 'custom' => $token
        ]);
        $paymentgateway->cancelurl = $CFG->wwwroot;
        $paymentgateway->strcontinue = get_string('continuetocourse');
        $paymentgateway->successurl = new moodle_url('/enrol/payment/gateway/notificationpaypal.php');

        $inputs = helper::standard_input($cost);

        $transferinstructions = helper::get_transfer_instructions(
            $cost->subtotal,
            $course->fullname,
            $course->shortname
        );

        $multipleregicon = $OUTPUT->help_icon('multipleregistration', 'enrol_payment');

        $params = json_encode([
            'price' => $price,
            'symbol' => $symbol,
            'prepaytoken' => $token,
            'useremail' => $USER->email,
            'instanceid' => $instanceid,
            'shippingaddressrequired' => ($requireshipping == 1),
            'stripepublishablekey' => $config->stripekey,
            'stripelogo' => helper::get_stripe_logo_url(),
            'validatezipcode' => ($config->validatezipcode == 1),
            'billingaddressrequired' => ($config->addressrequired == 1),
            'discountcoderequired' => ($config->codeisrequired == 1),
            'stringkeys' => json_encode(helper::get_string_keys()),
        ]);

        $PAGE->requires->js_call_amd('enrol_payment/enrolpage', 'init', [$params]);
        $PAGE->requires->css('/enrol/payment/less/styles.css');

        $totemplate = [
            'user' => $USER,
            'cost' => (array) $cost,
            'paymentgateway' => $paymentgateway,
            'transferinstructions' => $transferinstructions,
            'multipleregicon' => $multipleregicon,
            'instanceid' => $instanceid,
            'courseid' => $course->id,
            'prepaytoken' => $token,
            'hastax' => ($cost->tax * 100) > 0,
            'taxpercent' => $cost->tax,
            'taxstring' => $cost->taxstring,
            'paypalenabled' => $config->haspaypal,
            'stripeenabled' => $config->hasstripe,
            'allowdiscount' => $config->allowdiscount,
            'allowmultiple' => $config->allowmultiple,
            'single' => $config->allowmultiple <= 0,
            'gatewaysenabled' => ($config->haspaypal || $config->hasstripe),
            'hasbothpayments' => ($config->haspaypal && $config->hasstripe),
            'inputs' => $inputs,
            'userjson' => json_encode((array)$user),
            'costjson' => json_encode((array)$cost)
        ];

        return $totemplate;
    }

    private function check_user_access() {
        require_login();
        if (isguestuser()) {
            print_error('guestsarenotallowed');
        }
    }

    private function load_user_profile() {
        global $USER;
        profile_load_data($USER);
    }

    private function allow_payment_enrollment() {
        return get_config('enrol_payment', 'status');
    }

    private function prepare_user_data() {
        global $USER;

        return (object)[
            'id' => $USER->id,
            'email' => $USER->email,
            'firstname' => $USER->firstname,
            'lastname' => $USER->lastname,
            'userfullname' => fullname($USER),
            'taxregion' => $USER->profile_field_taxregion ?? ''
        ];
    }

    /**
     * Returns multiple configuration values.
     *
     * @param  stdClass $instance
     * @return stdClass
     */
    private function get_settings() {

        $config = new stdClass;
        $config->discounttype = 1;
        $config->codeisrequired = 1;

        $config->taxinfo = (object) $this->get_tax_info();
        $config->allowmultiple = (bool) $this->instance->customint5;
        $config->threshold = 1;

        $config->haspaypal = (bool) trim(get_config('enrol_payment', 'paypalbusiness'));
        $config->paypalaccount = get_config('enrol_payment', 'paypalbusiness');
        $config->paypalclientid = get_config('enrol_payment', 'paypalclientid');
        $config->allowcard = get_config('enrol_payment', 'paypalallowcard');

        $stripesecret = get_config('enrol_payment', 'stripesecretkey');
        $config->stripekey = get_config('enrol_payment', 'stripepublishablekey');
        $config->hasstripe = ((bool) trim($stripesecret)) && ((bool) trim($config->stripekey));

        $config->transferinstructions = get_config('enrol_payment', 'transferinstructions');
        $config->validatezipcode = get_config('enrol_payment', 'validatezipcode');
        $config->addressrequired = get_config('enrol_payment', 'billingaddress');
        $config->allowexistingenrol = (bool) $this->instance->customint6;
        $config->allownewenrol = (bool) $this->instance->customint7;
        $config->allowdiscount = (bool) $this->instance->customint3;

        return $config;
    }

    /**
     * Return the tax amount.
     *
     * @param  string $tax
     * @param  string $userfield
     *
     * @return array
     */
    private function get_tax_amount(string $tax, string $userfield, bool $fixedtax = false) {

        $pieces = explode(":", $tax);
        if (count($pieces) != 2) {
            debugging('Incorrect tax definition format.');
        }

        $taxregion = strtolower(trim($pieces[0]));
        $taxrate = trim($pieces[1]);

        // If the user country and the tax country does not match, return with empty values.
        if (stripos($taxregion, strtolower(trim($userfield))) === false && !$fixedtax) {
            return ['taxpercent' => 0, 'taxstring' => ''];
        }

        if (!is_numeric($taxrate)) {
            debugging('Encountered non-numeric tax value.');
        }

        try {
            $floattaxrate = floatval($taxrate);
            return [
                'taxpercent' => $floattaxrate,
                'taxstring' => get_string('tax', 'enrol_payment') . ' (' . floor($floattaxrate * 100) . '%)'
            ];
        } catch (Object $e) {
            debugging("Could not convert tax value for $e->getMessage() into a float.");
        }
    }

    /**
     * Returns the tax info.
     *
     * @return array
     */
    private function get_tax_info() {
        global $USER;
        profile_load_data($USER);

        // If the option is disabled, return.
        $taxtype = get_config('enrol_payment', 'definetaxes');

        if (!$taxtype) {
            return ['taxpercent' => 0, 'taxstring' => ''];
        }

        $taxdefs = get_config('enrol_payment', 'taxdefinitions');
        $countrytax = get_config('enrol_payment', 'countrytax');
        $fixedtax = get_config('enrol_payment', 'fixedtax');

        // If country tax are set and the user country is empty. Force user to edit his profile.
        if (!empty($countrytax) && empty($USER->country)) {
            $urltogo = new moodle_url('/user/edit.php', ['id' => $USER->id]);
            redirect($urltogo, 'You must choose your country', null, \core\output\notification::NOTIFY_WARNING);
        }

        if ($taxtype == 'fixedtax' && !empty($fixedtax)) {
            return $this->get_tax_amount($fixedtax, '', true);
        }

        if (!empty($countrytax)) {
            $taxholder = $this->get_tax_amount($countrytax, $USER->country);
            return $taxholder;
        }

        // If the tax field is not set, let's not calculate taxes.
        if (!isset($USER->profile_field_taxregion)) {
            return ['taxpercent' => 0, 'taxstring' => ''];
        }

        // If the tax country is not empty, use it. Otherwise use the tax region.
        $taxdefs = get_config('enrol_payment', 'taxdefinitions');
        $taxdeflines = explode("\n", $taxdefs);

        // Return if this is empty.
        if (empty($taxdefs)) {
            return ['taxpercent' => 0, 'taxstring' => ''];
        }

        foreach ($taxdeflines as $taxline) {
            $taxholder = $this->get_tax_amount($taxline, $USER->profile_field_taxregion);
            if (!empty($taxholder['taxpercent'])) {
                break;
            }
        }

        return $taxholder;
    }
}
