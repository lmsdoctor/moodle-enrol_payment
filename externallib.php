<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Notify the user that their PayPal payment is pending
 *
 * @package    enrol_payment
 * @copyright  2020 LMS Doctor
 * @author     Seth Yoder <seth.a.yoder@gmail.com>
 * @author     Andres Ramos <andres.ramos@lmsdoctor.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/externallib.php');

use enrol_payment\helper;

class enrol_payment_external extends external_api {

    /**
     * Returns description of method parameters.
     *
     * @return external_function_parameters
     */
    public static function check_enrollment_email_parameters() {
        return new external_function_parameters(
            [
                'email' => new external_value(PARAM_EMAIL, 'The user email'),
                'enrolid' => new external_value(PARAM_INT, 'Enrol id from the enrol table'),
                'prepaytoken' => new external_value(PARAM_NOTAGS, 'The prepay token'),
                'action' => new external_value(PARAM_INT, 'Enrol action'),
            ]
        );
    }

    /**
     * Check discount.
     *
     * @param  int $enrolid
     * @param  string $email
     * @param  string $prepaytoken
     * @param  string $discountcode
     * @return array
     */
    public static function check_enrollment_email(string $email, int $enrolid, string $prepaytoken, int $action) {
        global $DB;

        // Parameters validation.
        $params = (object) self::validate_parameters(
            self::check_enrollment_email_parameters(),
            [
                'email' => $email,
                'enrolid' => $enrolid,
                'prepaytoken' => $prepaytoken,
                'action' => $action,
            ]
        );

        // Check if the user exists.
        $user = $DB->get_record('user', ['email' => urldecode($params->email)], 'id, email, firstname, lastname');
        if (empty($user)) {
            return helper::response_externallib('thisuserdoesnotexist', 'user is null');
        }

        // Get enrol payment session.
        $payment = helper::get_payment_from_token($params->prepaytoken);
        $multipleuserids = $payment->multipleuserids ?? "";
        if (empty($multipleuserids)) {
            $multipleuserids = [];
        } else {
            $multipleuserids = explode(',', $multipleuserids);
        }

        // Validation if the user is already in the list of buyer users.
        $istoadd = ((int)$params->action) == 1;
        $buyerslist = in_array($user->id, $multipleuserids, true);

        if ($istoadd && $buyerslist) {
            // Error: This user already exists in the shopping list.
            return helper::response_externallib('thisusershopping', 'error');
        } else if (!$istoadd && !$buyerslist) {
            // Error: This user was already removed from the list.
            // $smg = get_string('alreadyremoved', 'enrol_payment', fullname($user));
            // throw new moodle_exception('alreadyremoved', 'enrol_payment');
            return helper::response_externallib('alreadyremoved', 'error');
        } else if (!$istoadd && $buyerslist && count($multipleuserids) == 1) {
            // Error: There should be at least 1 user to register.
            // $smg = get_string('notenoughunits', 'enrol_payment');
            // throw new moodle_exception('atleastone', 'enrol_payment');
            return helper::response_externallib('atleastone', 'error');
        }

        if ($istoadd) {
            // Add user shopping.
            array_push($multipleuserids, $user->id);
            // update units in the payment session
            $payment->units = count($multipleuserids) ?? 1;
        } else if (!$istoadd) {
            // Remove user shopping.
            $index = array_search($user->id, $multipleuserids, true);
            unset($multipleuserids[$index]);
            // update units in the payment session
            $payment->units = count($multipleuserids) ?? 1;
        }

        // Discount coupon validation.
        $coupon = new stdClass;
        $coupon->amount = 0;
        $coupon->type = 0;
        $coupon->threshold = 0;
        if ($payment->coupon) {
            // Get coupon config.
            $coupon = $DB->get_record('enrol_payment_discountcode', ['code' => trim($payment->coupon)]);
            $validate = helper::validate_coupon($coupon, $payment->courseid, true, true);
            if (is_string($validate)) {
                return $validate;
            }
            $payment->codegiven = $validate;
            $payment->coupon = $coupon->code;
        }


        // Update record of payment session.
        $payment->multiple = true;
        $payment->multipleuserids = implode(",", $multipleuserids);
        $DB->update_record('enrol_payment_session', $payment);

        // Calculate payment cost.
        $cost = helper::calculate_cost($coupon, $payment);
        $cost->user = "";
        $cost->prepaytoken = $params->prepaytoken;
        $cost->code = $params->discountcode;
        if ($istoadd) {
            $cost->user = json_encode((array) $user);
        } else if (!$istoadd) {
            $cost->userremoveid = $user->id;
        }
        return json_encode((array)$cost);
    }

    /**
     * Returns description of method result value.
     *
     * @return external_description
     */
    public static function check_enrollment_email_returns() {
        return new external_value(PARAM_NOTAGS, 'Returns an array with the calculated costs');
    }

    /**
     * Returns description of method parameters.
     *
     * @return external_function_parameters
     */
    public static function check_discount_parameters() {
        return new external_function_parameters(
            [
                'enrolid' => new external_value(PARAM_INT, 'Enrol id from the enrol table'),
                'prepaytoken' => new external_value(PARAM_NOTAGS, 'The prepay token'),
                'discountcode' => new external_value(PARAM_NOTAGS, 'The discount code'),
            ]
        );
    }

    /**
     * Check discount.
     *
     * @param  int $enrolid
     * @param  string $prepaytoken
     * @param  string $discountcode
     * @return array
     */
    public static function check_discount($enrolid, $prepaytoken, $discountcode) {
        global $DB;

        // Parameters validation.
        $params = (object) self::validate_parameters(
            self::check_discount_parameters(),
            [
                'enrolid' => $enrolid,
                'prepaytoken' => $prepaytoken,
                'discountcode' => $discountcode,
            ]
        );

        $discountcode = trim($params->discountcode);

        // Get enrollment record from the table.
        $select = sprintf("%s = :code", $DB->sql_compare_text('code'));
        $coupon = $DB->get_record_select('enrol_payment_discountcode', $select, array('code' => $discountcode));
        $payment = helper::get_payment_from_token($params->prepaytoken);

        // Coupon validation.
        $validate = helper::validate_coupon($coupon, $payment->courseid, true);
        if (is_string($validate)) {
            return $validate;
        }

        $payment->codegiven = $validate;
        $payment->coupon = $coupon->code;
        $DB->update_record('enrol_payment_session', $payment);

        $cost = helper::calculate_cost($coupon, $payment, true);

        return json_encode((array) $cost);
    }

    /**
     * Returns description of method result value.
     *
     * @return external_description
     */
    public static function check_discount_returns() {
        return new external_value(PARAM_NOTAGS, 'Returns an array with the calculated costs');
    }

    /**
     * Returns description of method parameters.
     *
     * @return external_function_parameters
     */
    public static function multiple_enrollment_parameters() {
        return new external_function_parameters(
            [
                'enrolid' => new external_value(PARAM_INT, 'Enrol id from the enrol table'),
                'prepaytoken' => new external_value(PARAM_NOTAGS, 'The prepay token'),
                'emails' => new external_value(PARAM_NOTAGS, 'Emails'),
                'ipnid' => new external_value(PARAM_RAW, 'IPN ID'),
                'symbol' => new external_value(PARAM_NOTAGS, 'Symbol'),
            ]
        );
    }

    /**
     * Validate multiple enrollments.
     *
     * @param  int $enrolid
     * @param  string $prepaytoken
     * @param  string $emails
     * @param  int $ipnid
     * @param  string $symbol
     * @return string
     */
    public static function multiple_enrollment($enrolid, $prepaytoken, $emails, $ipnid, $symbol) {
        global $DB, $CFG;

        // Parameters validation.
        $params = (object) self::validate_parameters(
            self::multiple_enrollment_parameters(),
            [
                'enrolid' => $enrolid,
                'prepaytoken' => $prepaytoken,
                'emails' => $emails,
                'ipnid' => $ipnid,
                'symbol' => $symbol,
            ]
        );

        return json_encode((array) $params);
    }

    /**
     * Returns description of method result value.
     *
     * @return external_description
     */
    public static function multiple_enrollment_returns() {
        return new external_value(PARAM_RAW, 'Returns an array with the multiple enrollment information');
    }

    /**
     * Returns description of method parameters.
     *
     * @return external_function_parameters
     */
    public static function single_enrollment_parameters() {
        return new external_function_parameters(
            [
                'enrolid' => new external_value(PARAM_INT, 'Enrol id from the enrol table'),
                'prepaytoken' => new external_value(PARAM_NOTAGS, 'The prepay token'),
            ]
        );
    }

    /**
     * Validate single enrollments.
     *
     * @param  int $enrolid
     * @param  string $prepaytoken
     * @return string
     */
    public static function single_enrollment($enrolid, $prepaytoken) {
        global $DB;

        // Parameters validation.
        $params = (object) self::validate_parameters(
            self::single_enrollment_parameters(),
            [
                'enrolid' => $enrolid,
                'prepaytoken' => $prepaytoken,
            ]
        );

        return json_encode((array) $params);
    }

    /**
     * Returns description of method result value.
     *
     * @return external_description
     */
    public static function single_enrollment_returns() {
        return new external_value(PARAM_RAW, 'Returns an array with the payment enrollment information');
    }

    /**
     * Returns description of method parameters.
     *
     * @return external_function_parameters
     */
    public static function check_enrol_parameters() {
        return new external_function_parameters(
            [
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'paymentid' => new external_value(PARAM_INT, 'Payment id'),
            ]
        );
    }

    /**
     * Validate single enrollments.
     *
     * @param  int $courseid
     * @param  string $paymentid
     * @return string
     */
    public static function check_enrol($courseid, $paymentid) {
        global $DB, $USER;

        // Parameters validation.
        $params = (object) self::validate_parameters(
            self::check_enrol_parameters(),
            [
                'courseid' => $courseid,
                'paymentid' => $paymentid,
            ]
        );

        $context = context_course::instance($params->courseid, MUST_EXIST);

        if (is_enrolled($context, $USER, '', true)) {
            return json_encode([
                'status' => 'success',
                'result' => true
            ]);
        } else if (helper::payment_pending($params->paymentid)) {
            return json_encode([
                'status' => 'success',
                'result' => false,
                'reason' => 'Pending'
            ]);
        } else {
            $reason = helper::get_payment_status($params->paymentid);
            return json_encode([
                'status' => 'success',
                'result' => false,
                'reason' => $reason
            ]);
        }
    }

    /**
     * Returns description of method result value.
     *
     * @return external_description
     */
    public static function check_enrol_returns() {
        return new external_value(PARAM_RAW, 'Returns an array with the check enrol result');
    }
}
